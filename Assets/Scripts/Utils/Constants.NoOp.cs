﻿using System.Diagnostics.CodeAnalysis;

public partial class Constants
{
  /// <summary>
  /// A 0-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  public static void NoOp() { }
}

public partial class Constants<T>
{
  /// <summary>
  /// A 1-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T arg)
  { }
}

public partial class Constants<T1, T2>
{
  /// <summary>
  /// A 2-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2)
  { }
}

public partial class Constants<T1, T2, T3>
{
  /// <summary>
  /// A 3-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3)
  { }
}

public partial class Constants<T1, T2, T3, T4>
{
  /// <summary>
  /// A 4-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5>
{
  /// <summary>
  /// A 5-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6>
{
  /// <summary>
  /// A 6-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7>
{
  /// <summary>
  /// A 7-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8>
{
  /// <summary>
  /// A 8-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9>
{
  /// <summary>
  /// A 9-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
{
  /// <summary>
  /// A 10-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
{
  /// <summary>
  /// A 11-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
{
  /// <summary>
  /// A 12-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11,
  T12, T13>
{
  /// <summary>
  /// A 13-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11,
  T12, T13, T14>
{
  /// <summary>
  /// A 14-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13,
    T14 arg14)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11,
  T12, T13, T14, T15>
{
  /// <summary>
  /// A 15-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13,
    T14 arg14, T15 arg15)
  { }
}

public partial class Constants<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11,
  T12, T13, T14, T15, T16>
{
  /// <summary>
  /// A 16-argument no-op function. Use this to initialize non-null delegates
  /// without runtime allocation.
  /// </summary>
  [SuppressMessage("Style",
    "IDE0060:Remove unused parameter",
    Justification = "NoOp functions don't do anything by definition")]
  public static void NoOp(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6,
    T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13,
    T14 arg14, T15 arg15, T16 arg16)
  { }
}