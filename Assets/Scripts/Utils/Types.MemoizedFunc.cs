﻿using System.Collections.Concurrent;
using System;

/// <summary>
/// Encapsulates a System.Func that has 1 parameter and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<T1, TResult> MemoizedResults
    = new ConcurrentDictionary<T1, TResult>();
  protected Func<T1, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 1-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, TResult> From(
    Func<T1, TResult> func)
  {
    return new MemoizedFunc<T1, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1)
  {
    return MemoizedResults.GetOrAdd(arg1,
      _func(arg1));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 2 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2), TResult>();
  protected Func<T1, T2, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 2-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, TResult> From(
    Func<T1, T2, TResult> func)
  {
    return new MemoizedFunc<T1, T2, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2),
      _func(arg1, arg2));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 3 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3), TResult>();
  protected Func<T1, T2, T3, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 3-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, TResult> From(
    Func<T1, T2, T3, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3),
      _func(arg1, arg2, arg3));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 4 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4), TResult>();
  protected Func<T1, T2, T3, T4, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 4-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, TResult> From(
    Func<T1, T2, T3, T4, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4),
      _func(arg1, arg2, arg3, arg4));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 5 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5), TResult>();
  protected Func<T1, T2, T3, T4, T5, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 5-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, TResult> From(
    Func<T1, T2, T3, T4, T5, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5),
      _func(arg1, arg2, arg3, arg4, arg5));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 6 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 6-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6),
      _func(arg1, arg2, arg3, arg4, arg5, arg6));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 7 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 7-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 8 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 8-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 9 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 9-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 10 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 10-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 11 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 11-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10, arg11),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 12 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 12-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10, arg11, arg12),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11,
        arg12));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 13 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 13-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12,
    T13 arg13)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10, arg11, arg12, arg13),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11,
        arg12, arg13));
  }
}


/// <summary>
/// Encapsulates a System.Func that has 14 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 14-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12,
    T13 arg13, T14 arg14)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10, arg11, arg12, arg13, arg14),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11,
        arg12, arg13, arg14));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 15 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 15-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12,
    T13 arg13, T14 arg14, T15 arg15)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11,
        arg12, arg13, arg14, arg15));
  }
}

/// <summary>
/// Encapsulates a System.Func that has 16 parameters and its memoized results.
/// This class is best used to encapsulate pure functions.
/// </summary>
public partial class MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TResult>
{
  /// <summary>
  /// The memoized results of the encapsulated function.
  /// </summary>
  public ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), TResult> MemoizedResults
    = new ConcurrentDictionary<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), TResult>();
  protected Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TResult> _func = null;

  /// <summary>
  /// Creates a MemoizedFunc from a 16-argument function.
  /// </summary>
  /// <returns>A new MemoizedFunc.</returns>
  public static MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TResult> From(
    Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TResult> func)
  {
    return new MemoizedFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TResult>
    { _func = func };
  }

  /// <summary>
  /// If the current combination of arguments does not already have a
  /// matching result memoized, invokes the encapsulated function with the
  /// current combination of arguments and memoize the result. Returns the
  /// memoized result that match the current combination of arguments.
  /// </summary>
  /// <returns>The new value, or the memoized value that matches the current
  /// combination of arguments</returns>
  public virtual TResult Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12,
    T13 arg13, T14 arg14, T15 arg15, T16 arg16)
  {
    return MemoizedResults.GetOrAdd((arg1, arg2, arg3, arg4, arg5, arg6, arg7,
      arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16),
      _func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11,
        arg12, arg13, arg14, arg15, arg16));
  }
}