﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class ResetTransformHasChanged : MonoBehaviour
{
  [AutoProperty] public new Transform transform = null;

  void LateUpdate() { if (transform.hasChanged) transform.hasChanged = false; }
}
