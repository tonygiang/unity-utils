﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System;

public class LocalRotateTransform : RuntimeExtensionBehaviour<Transform>
{
  [field: SerializeField, OverrideLabel("Angular Velocity")]
  public Vector3 AngularVelocity { get; set; }
  [field: SerializeField, OverrideLabel("Relative To")]
  public Space RelativeTo { get; set; }
  public bool ScaledTime = true;
  Dictionary<bool, Func<float>> timeGetters
    = new Dictionary<bool, Func<float>>()
    {
      [true] = () => Time.deltaTime,
      [false] = () => Time.unscaledDeltaTime
    };

  void UpdateAction()
  {
    transform.Rotate(AngularVelocity * timeGetters[ScaledTime](), RelativeTo);
  }

  void OnEnable() => GameLoopRunner.Instance.UpdateActions.Add(UpdateAction);

  void OnDisable() => GameLoopRunner.Instance.UpdateRemovalQueue
    .Enqueue(UpdateAction);
}
