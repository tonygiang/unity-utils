﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class TransformFunctions : MonoBehaviour
{
  [AutoProperty] public new Transform transform;

  public void MovePositionX(float x) =>
    transform.position = transform.position.SetX(x);

  public void MovePositionY(float y) =>
    transform.position = transform.position.SetY(y);

  public void MovePositionZ(float z) =>
    transform.position = transform.position.SetZ(z);
}
