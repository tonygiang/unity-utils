﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyBox;

public class GameLoopRunner : MonoBehaviour
{
  public static GameLoopRunner Instance = null;
  public List<Action> UpdateActions = new List<Action>();
  public Queue<Action> UpdateRemovalQueue = new Queue<Action>(10);
  public List<Action> FixedUpdateActions = new List<Action>();
  public Queue<Action> FixedUpdateRemovalQueue = new Queue<Action>(10);
  public List<Action> LateUpdateActions = new List<Action>();
  public Queue<Action> LateUpdateRemovalQueue = new Queue<Action>(10);
  public Dictionary<MonoBehaviour, List<Coroutine>> Coroutines
    = new Dictionary<MonoBehaviour, List<Coroutine>>();

  void Awake()
  {
    RegisterSingleton();
  }

  public void RegisterSingleton() => Instance = this;

  void Update()
  {
    while (UpdateRemovalQueue.Count > 0)
      UpdateActions.Remove(UpdateRemovalQueue.Dequeue());
    foreach (var r in UpdateActions) r.Invoke();
  }

  void FixedUpdate()
  {
    while (FixedUpdateRemovalQueue.Count > 0)
      FixedUpdateActions.Remove(FixedUpdateRemovalQueue.Dequeue());
    foreach (var r in FixedUpdateActions) r.Invoke();
  }

  void LateUpdate()
  {
    while (LateUpdateRemovalQueue.Count > 0)
      LateUpdateActions.Remove(LateUpdateRemovalQueue.Dequeue());
    foreach (var r in LateUpdateActions) r.Invoke();
  }

  public void UpdateOnce(Action action)
  {
    Action wrapperAction = null;
    wrapperAction = () =>
    { action(); UpdateRemovalQueue.Enqueue(wrapperAction); };
    UpdateActions.Add(wrapperAction);
  }

  public void FixedUpdateOnce(Action action)
  {
    Action wrapperAction = null;
    wrapperAction = () =>
    { action(); FixedUpdateRemovalQueue.Enqueue(wrapperAction); };
    FixedUpdateActions.Add(wrapperAction);
  }

  public void LateUpdateOnce(Action action)
  {
    Action wrapperAction = null;
    wrapperAction = () =>
    { action(); LateUpdateRemovalQueue.Enqueue(wrapperAction); };
    LateUpdateActions.Add(wrapperAction);
  }

  public Coroutine StartCoroutine(MonoBehaviour source, IEnumerator routine)
  {
    var createdCoroutine = StartCoroutine(routine);
    Coroutines.GetOrAdd(source, MakeCoroutineList).Add(createdCoroutine);
    return createdCoroutine;
  }

  public void StopCoroutine(MonoBehaviour source, Coroutine coroutine)
  {
    StopCoroutine(coroutine);
    Coroutines.GetOrAdd(source, MakeCoroutineList).Remove(coroutine);
  }

  public void StopAllCoroutines(MonoBehaviour source)
  {
    Coroutines.GetOrAdd(source, MakeCoroutineList).ForEach(StopCoroutine);
    Coroutines[source].Clear();
  }

  static List<Coroutine> MakeCoroutineList(object _) => new List<Coroutine>();
}
