﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.UI;

public class CameraHorizontalFOVInitializer : ExtensionBehaviour<Camera>
{
  public AspectRatioFitter ReferenceFitter;
  public float CurrentRatio => BaseComp.pixelRect.width
    / BaseComp.pixelRect.height;
  public float TargetHorizontalFOV;

  void Awake()
  {
    if (CurrentRatio >= ReferenceFitter.aspectRatio) return;
    BaseComp.fieldOfView = Camera.HorizontalToVerticalFieldOfView(
        TargetHorizontalFOV,
        Mathf.Min(CurrentRatio, ReferenceFitter.aspectRatio));
  }

  [ButtonMethod]
  public void Fill() => TargetHorizontalFOV = Camera
    .VerticalToHorizontalFieldOfView(BaseComp.fieldOfView,
      CurrentRatio);
}
