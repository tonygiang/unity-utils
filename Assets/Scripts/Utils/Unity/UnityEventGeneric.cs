﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Numerics;
using System.Collections.Specialized;
using System.ComponentModel;

[Serializable]
public class UnityEventVector4 : UnityEvent<UnityEngine.Vector4> { }

[Serializable]
public class UnityEventVector3 : UnityEvent<UnityEngine.Vector3> { }

[Serializable]
public class UnityEventVector2 : UnityEvent<UnityEngine.Vector2> { }

[Serializable] public class UnityEventObject : UnityEvent<object> { }

[Serializable] public class UnityEventString : UnityEvent<string> { }
[Serializable] public class UnityEventInt : UnityEvent<int> { }

[Serializable] public class UnityEventBool : UnityEvent<bool> { }

[Serializable] public class UnityEventFloat : UnityEvent<float> { }

[Serializable] public class UnityEventDouble : UnityEvent<double> { }

[Serializable] public class UnityEventBigInteger : UnityEvent<BigInteger> { }

[Serializable]
public class UnityEventStrings : UnityEvent<IEnumerable<string>> { }

[Serializable]
public class UnityEventTuples2Int
  : UnityEvent<IEnumerable<ValueTuple<int, int>>>
{ }

[Serializable]
public class UnityEventComponent : UnityEvent<UnityEngine.Component> { }

[Serializable] public class UnityEventCollider2D : UnityEvent<Collider2D> { }

[Serializable] public class UnityEventCollision2D : UnityEvent<Collision2D> { }

[Serializable] public class UnityEventCollider : UnityEvent<Collider> { }

[Serializable] public class UnityEventCollision : UnityEvent<Collision> { }

[Serializable]
public class UnityEventTransforms : UnityEvent<IEnumerable<Transform>> { }

[Serializable]
public class UnityEventNotifyCollectionChangedEventArgs
  : UnityEvent<NotifyCollectionChangedEventArgs>
{ }

[Serializable]
public class UnityEventListChangedEventArgs : UnityEvent<ListChangedEventArgs>
{ }

[Serializable]
public class UnityEventPointerEventData : UnityEvent<PointerEventData> { }