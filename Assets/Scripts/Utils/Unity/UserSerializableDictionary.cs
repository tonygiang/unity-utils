﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System;
using System.Collections.Specialized;
using MyBox;
using Utils;
using System.Linq;
using UnityEngine.UI;

[Serializable]
public class SerializableDictionaryStringString
  : SerializableDictionaryBase<string, string>
{ }

[Serializable]
public class SerializableDictionaryIntString
  : SerializableDictionaryBase<int, string>
{ }

[Serializable]
public class SerializableDictionaryStringInt
  : SerializableDictionaryBase<string, int>
{ }

[Serializable]
public class SerializableDictionaryCharInt
  : SerializableDictionaryBase<char, int>
{ }

[Serializable]
public class SerializableDictionaryStringSprite
  : SerializableDictionaryBase<string, Sprite>
{ }

[Serializable]
public class SerializableDictionaryStringUnityEventString
  : SerializableDictionaryBase<string, UnityEventString>
{ }

[Serializable]
public class SerializableDictionaryIntUnityEventInt
  : SerializableDictionaryBase<int, UnityEventInt>
{ }

[Serializable]
public class SerializableDictionaryBoolUnityEventBool
  : SerializableDictionaryBase<bool, UnityEventBool>
{
  public SerializableDictionaryBoolUnityEventBool()
  {
    this.Add(true, new UnityEventBool());
    this.Add(false, new UnityEventBool());
  }
}

[Serializable]
public class SerializableDictionaryRangedFloatUnityEventFloat
  : SerializableDictionaryBase<RangedFloat, UnityEventFloat>
{ }

[Serializable]
public class SerializableDictionaryRangedIntUnityEventInt
  : SerializableDictionaryBase<RangedInt, UnityEventInt>
{ }

[Serializable]
public class SerializableDictionaryIntButton
  : SerializableDictionaryBase<int, Button>
{ }

[Serializable]
public class SerializableDictionaryStringUnityObject
  : SerializableDictionaryBase<string, UnityEngine.Object>
{ }

[Serializable]
public class SerializableDictionaryStringGameObject
  : SerializableDictionaryBase<string, GameObject>
{ }


[Serializable]
public class SerializableDictionaryStringScriptableObject
  : SerializableDictionaryBase<string, ScriptableObject>
{ }

[Serializable]
public class SerializableDictionaryNotifyCollectionChanged
  : SerializableDictionaryBase<NotifyCollectionChangedAction,
    UnityEventNotifyCollectionChangedEventArgs>
{
  public SerializableDictionaryNotifyCollectionChanged() => Algorithm
    .ValuesOf<NotifyCollectionChangedAction>()
    .Select(a => (a, new UnityEventNotifyCollectionChangedEventArgs()))
    .ForEach(t => this.Add(t.a, t.Item2));
}

[Serializable]
public class SerializableDictionaryStringVariableString
  : SerializableDictionaryBase<string, VariableString>
{ }

[Serializable]
public class SerializableDictionaryStringVariableBool
  : SerializableDictionaryBase<string, VariableBool>
{ }

[Serializable]
public class SerializableDictionaryStringVariableInt
  : SerializableDictionaryBase<string, VariableInt>
{ }

[Serializable]
public class SerializableDictionaryStringVariableFloat
  : SerializableDictionaryBase<string, VariableFloat>
{ }

[Serializable]
public class SerializableDictionarySystemLanguageSprite
  : SerializableDictionaryBase<SystemLanguage, Sprite>
{ }