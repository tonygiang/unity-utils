﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Linq;
using Utils;

public class GameObjectCollection : MonoBehaviour
{
  public GameObject[] Values = new GameObject[0];

  public void ToggleAll(bool state) =>
    Values.ForEach(go => go.SetActive(state));

  public void EnableFirst(int number) =>
    Values.Take(number).ForEach(go => go.SetActive(true));

  public void EnableFirst(string numberText) => EnableFirst(numberText.ToInt());

  public void EnableLast(int number) =>
    Values.Skip(Values.Length - number).ForEach(go => go.SetActive(true));

  public void EnableLast(string numberText) => EnableLast(numberText.ToInt());

  public void DisableFirst(int number) =>
    Values.Take(number).ForEach(go => go.SetActive(false));

  public void DisableFirst(string numberText) =>
    DisableFirst(numberText.ToInt());

  public void DisableLast(int number) =>
    Values.Skip(Values.Length - number).ForEach(go => go.SetActive(false));

  public void DisableLast(string numberText) => DisableLast(numberText.ToInt());

}
