﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectInstance : MonoBehaviour
{
  public GameObjectSO Source = null;

  void Awake() => Source.Value = gameObject;
}
