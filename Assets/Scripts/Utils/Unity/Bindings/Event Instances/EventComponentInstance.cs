﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;

public class EventComponentInstance : EventInstance<Component>
{
  public EventComponentSO[] Sources = new EventComponentSO[0];
  public override UnityEvent<Component> Event => _event;
  [SerializeField] UnityEventComponent _event = new UnityEventComponent();

  void Awake() => Sources.ForEach(RegisterTo);

  void OnDestroy() { Sources.ForEach(UnregisterFrom); _event = null; }

  public void RegisterTo(EventComponentSO ev) => ev.Listeners
    .AddListener(Event.Invoke);

  public void RegisterTo(EventComponentInstance ev) => ev.Event
    .AddListener(Event.Invoke);

  public void UnregisterFrom(EventComponentSO ev) => ev.Listeners
    .RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventComponentInstance ev) => ev.Event
    .RemoveListener(Event.Invoke);
}
