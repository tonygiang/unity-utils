﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MyBox;

public class EventInstance : MonoBehaviour
{
  public EventSO[] Sources = new EventSO[0];
  public UnityEvent Event = new UnityEvent();

  void Awake() => Sources.ForEach(RegisterTo);

  void OnDestroy() { Sources.ForEach(UnregisterFrom); Event = null; }

  public void RegisterTo(EventSO ev) => ev.Listeners.AddListener(Event.Invoke);

  public void RegisterTo(EventInstance ev) =>
    ev.Event.AddListener(Event.Invoke);

  public void UnregisterFrom(EventSO ev) =>
    ev.Listeners.RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventInstance ev) =>
    ev.Event.RemoveListener(Event.Invoke);

  public void RemoveAllListeners() => Event.RemoveAllListeners();

  [ButtonMethod]
  public void Invoke() => Event.Invoke();
}

public abstract class EventInstance<T> : MonoBehaviour
{
  public abstract UnityEvent<T> Event { get; }

  public void Invoke(T value) => Event.Invoke(value);
}