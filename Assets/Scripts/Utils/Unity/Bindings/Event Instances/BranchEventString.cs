﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class BranchEventString : MonoBehaviour
{
  public EventStringSO[] Sources = new EventStringSO[0];
  public SerializableDictionaryStringUnityEventString Events
    = new SerializableDictionaryStringUnityEventString();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Branch));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Branch));
    Events = null;
  }

  public void Branch(string value)
  {
    if (!Events.ContainsKey(value)) return;
    Events[value].Invoke(value);
  }

  public void Branch(VariableString variable) => Branch(variable.Value);

  public void RegisterTo(EventStringSO ev) => ev.Listeners.AddListener(Branch);

  public void UnregisterFrom(EventStringSO ev) => ev.Listeners
    .RemoveListener(Branch);
}
