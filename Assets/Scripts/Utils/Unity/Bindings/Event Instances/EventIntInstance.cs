﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;

public class EventIntInstance : EventInstance<int>
{
  public EventIntSO[] Sources = new EventIntSO[0];
  public override UnityEvent<int> Event => _event;
  [SerializeField] UnityEventInt _event = new UnityEventInt();

  void Awake() => Sources.ForEach(RegisterTo);

  void OnDestroy() { Sources.ForEach(UnregisterFrom); _event = null; }

  public void Invoke(VariableInt variable) => Event.Invoke(variable.Value);

  public void RegisterTo(EventIntSO ev) => ev.Listeners
    .AddListener(Event.Invoke);

  public void RegisterTo(EventIntInstance ev) => ev.Event
    .AddListener(Event.Invoke);

  public void UnregisterFrom(EventIntSO ev) => ev.Listeners
    .RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventIntInstance ev) => ev.Event
    .RemoveListener(Event.Invoke);
}
