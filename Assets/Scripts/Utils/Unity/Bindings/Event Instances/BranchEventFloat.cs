﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

public class BranchEventFloat : MonoBehaviour
{
  public EventFloatSO[] Sources = new EventFloatSO[0];
  public SerializableDictionaryRangedFloatUnityEventFloat Events
    = new SerializableDictionaryRangedFloatUnityEventFloat();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Branch));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Branch));
    Events = null;
  }

  public void Branch(float value) => Events
    .Where(p => value.InRangeInclusive(p.Key))
    .ForEach(p => p.Value.Invoke(value));

  public void Branch(VariableFloat variable) => Branch(variable.Value);

  public void RegisterTo(EventFloatSO ev) => ev.Listeners.AddListener(Branch);

  public void UnregisterFrom(EventFloatSO ev) => ev.Listeners
    .RemoveListener(Branch);
}
