﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Newtonsoft.Json.Linq;
using System.Linq;

public class BranchEventJSONString : MonoBehaviour
{
  public EventStringSO[] Sources = new EventStringSO[0];
  public SerializableDictionaryStringUnityEventString Events
    = new SerializableDictionaryStringUnityEventString();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Branch));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Branch));
    Events = null;
  }

  public void Branch(string value)
  {
    var token = JToken.Parse(value);
    Events.Select(p => (token.SelectToken(p.Key), p.Value))
      .Where(p => p.Item1 != null)
      .ForEach(p => p.Value.Invoke(p.Item1.ToString()));
  }

  public void Branch(VariableString variable) => Branch(variable.Value);

  public void RegisterTo(EventStringSO ev) => ev.Listeners.AddListener(Branch);

  public void UnregisterFrom(EventStringSO ev) => ev.Listeners
    .RemoveListener(Branch);
}
