﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

public class BranchEnvironmentalDefines : MonoBehaviour
{
  public SerializableDictionaryBoolUnityEventBool OnDetect
    = new SerializableDictionaryBoolUnityEventBool();

  public string DetectingDefine = string.Empty;

  void Awake()
  {
    if (string.IsNullOrWhiteSpace(DetectingDefine)) return;
    if (!Utils.Unity.Constants.EnvironmentDefines.ContainsKey(DetectingDefine))
      return;
    OnDetect[Utils.Unity.Constants.EnvironmentDefines[DetectingDefine]]
      .Invoke(Utils.Unity.Constants.EnvironmentDefines[DetectingDefine]);
  }
}
