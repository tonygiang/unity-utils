﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;

public class EventDoubleInstance : EventInstance<double>
{
  public EventDoubleSO[] Sources = new EventDoubleSO[0];
  public override UnityEvent<double> Event => _event;
  [SerializeField] UnityEventDouble _event = new UnityEventDouble();

  void Awake() => Sources.ForEach(RegisterTo);

  void OnDestroy() { Sources.ForEach(UnregisterFrom); _event = null; }

  public void Invoke(VariableDouble variable) => Event.Invoke(variable.Value);

  public void RegisterTo(EventDoubleSO ev) => ev.Listeners
    .AddListener(Event.Invoke);

  public void RegisterTo(EventDoubleInstance ev) => ev.Event
    .AddListener(Event.Invoke);

  public void UnregisterFrom(EventDoubleSO ev) => ev.Listeners
    .RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventDoubleInstance ev) => ev.Event
    .RemoveListener(Event.Invoke);
}
