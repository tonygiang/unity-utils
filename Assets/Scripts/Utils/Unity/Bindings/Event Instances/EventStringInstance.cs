﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;

public class EventStringInstance : EventInstance<string>
{
  public EventStringSO[] Sources = new EventStringSO[0];
  public override UnityEvent<string> Event => _event;
  [SerializeField] UnityEventString _event = new UnityEventString();

  void Awake() => Sources.ForEach(RegisterTo);

  void OnDestroy() { Sources.ForEach(UnregisterFrom); _event = null; }

  public void Invoke(VariableString variable) => Event.Invoke(variable.Value);

  public void RegisterTo(EventStringSO ev) => ev.Listeners
    .AddListener(Event.Invoke);

  public void RegisterTo(EventStringInstance ev) => ev.Event
    .AddListener(Event.Invoke);

  public void UnregisterFrom(EventStringSO ev) => ev.Listeners
    .RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventStringInstance ev) => ev.Event
    .RemoveListener(Event.Invoke);
}
