﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;

public class EventBoolInstance : EventInstance<bool>
{
  public EventBoolSO[] Sources = new EventBoolSO[0];
  public override UnityEvent<bool> Event => _event;
  [SerializeField] UnityEventBool _event = new UnityEventBool();

  void Awake() => Sources.ForEach(RegisterTo);

  void OnDestroy() { Sources.ForEach(UnregisterFrom); _event = null; }

  public void Invoke(VariableBool variable) => Event.Invoke(variable.Value);

  public void RegisterTo(EventBoolSO ev) => ev.Listeners
    .AddListener(Event.Invoke);

  public void RegisterTo(EventBoolInstance ev) => ev.Event
    .AddListener(Event.Invoke);

  public void UnregisterFrom(EventBoolSO ev) => ev.Listeners
    .RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventBoolInstance ev) => ev.Event
    .RemoveListener(Event.Invoke);
}
