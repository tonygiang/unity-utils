﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

public class BranchEventInt : MonoBehaviour
{
  public EventIntSO[] Sources = new EventIntSO[0];
  public SerializableDictionaryRangedIntUnityEventInt Events
    = new SerializableDictionaryRangedIntUnityEventInt();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Branch));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Branch));
    Events = null;
  }

  public void Branch(int value) => Events
    .Where(p => value.InRangeInclusive(p.Key))
    .ForEach(p => p.Value.Invoke(value));

  public void Branch(VariableInt variable) => Branch(variable.Value);

  public void RegisterTo(EventIntSO ev) => ev.Listeners.AddListener(Branch);

  public void UnregisterFrom(EventIntSO ev) => ev.Listeners
    .RemoveListener(Branch);
}
