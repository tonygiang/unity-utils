﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;
using System.Numerics;

public class EventBigIntegerInstance : EventInstance<BigInteger>
{
  public EventBigIntegerSO[] Sources = new EventBigIntegerSO[0];
  public override UnityEvent<BigInteger> Event => _event;
  public UnityEventBigInteger _event = new UnityEventBigInteger();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Event.Invoke));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Event.Invoke));
    _event = null;
  }

  public void Invoke(VariableBigInteger variable) => Event
    .Invoke(variable.Value);

  public void RegisterTo(EventBigIntegerSO ev) => ev.Listeners
    .AddListener(Event.Invoke);

  public void RegisterTo(EventBigIntegerInstance ev) => ev.Event
    .AddListener(Event.Invoke);

  public void UnregisterFrom(EventBigIntegerSO ev) => ev.Listeners
    .RemoveListener(Event.Invoke);

  public void UnregisterFrom(EventBigIntegerInstance ev) => ev.Event
    .RemoveListener(Event.Invoke);
}
