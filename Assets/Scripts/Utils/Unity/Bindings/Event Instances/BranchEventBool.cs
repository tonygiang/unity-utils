﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class BranchEventBool : MonoBehaviour
{
  public EventBoolSO[] Sources = new EventBoolSO[0];
  public SerializableDictionaryBoolUnityEventBool Events
    = new SerializableDictionaryBoolUnityEventBool();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Branch));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Branch));
    Events = null;
  }

  public void Branch(bool value)
  {
    if (!Events.ContainsKey(value)) return;
    Events[value].Invoke(value);
  }

  public void Branch(VariableBool variable) => Branch(variable.Value);

  public void RegisterTo(EventBoolSO ev) => ev.Listeners.AddListener(Branch);

  public void UnregisterFrom(EventBoolSO ev) => ev.Listeners
    .RemoveListener(Branch);
}
