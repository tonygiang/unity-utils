﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryRenderTexture : MonoBehaviour
{
  public RenderTexture DescriptorSource;
  public Camera SourceCamera;
  public RenderTexture Value = null;

  void Awake() => Create();

  public void Create()
  {
    if (Value != null) return;
    Value = RenderTexture.GetTemporary(DescriptorSource.descriptor);
    SourceCamera.targetTexture = Value;
  }

  void OnDestroy()
  {
    RenderTexture.ReleaseTemporary(Value);
    Value = null;
  }
}
