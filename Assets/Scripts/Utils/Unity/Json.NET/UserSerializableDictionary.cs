﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System;

[Serializable]
public class SerializableDictionaryStringUnityEventJToken
  : SerializableDictionaryBase<string, UnityEventJToken>
{ }
