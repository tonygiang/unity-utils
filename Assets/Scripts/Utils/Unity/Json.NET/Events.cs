﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using Newtonsoft.Json.Linq;

[Serializable] public class UnityEventJToken : UnityEvent<JToken> { }
[Serializable] public class UnityEventJProperty : UnityEvent<JProperty> { }
[Serializable] public class UnityEventJObject : UnityEvent<JObject> { }
[Serializable] public class UnityEventJArray : UnityEvent<JObject> { }
[Serializable] public class UnityEventJValue : UnityEvent<JValue> { }