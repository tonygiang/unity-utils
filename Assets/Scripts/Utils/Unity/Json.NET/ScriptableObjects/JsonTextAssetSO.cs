﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JsonTextAssetSO")]
public class JsonTextAssetSO : JTokenSOBase
{
  public TextAsset Source = null;

  public override void OnEnable() { base.OnEnable(); Reload(); }

  public void Reload()
  { if (Source != null) Token.Value = JToken.Parse(Source.text); }
}
