﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JsonVariableStringSO")]
public class JsonVariableStringSO : JTokenSOBase
{
  public VariableString Source = null;
  public Newtonsoft.Json.Formatting WriteToSourceFormatting;

  public override void OnEnable()
  {
    base.OnEnable();
    Source.OnChange.Listeners.AddListener(UpdateToken);
    UpdateToken(Source.Value);
  }

  public override void OnDisable()
  {
    base.OnDisable();
    Source.OnChange.Listeners.RemoveListener(UpdateToken);
  }

  public void UpdateToken(string value)
  {
    if (value.Length == 0) { Token.Value = null; return; }
    Token.Value = JToken.Parse(value);
  }

  public void UpdateToken() => UpdateToken(Source.Value);

  [ButtonMethod]
  public void WriteToSource()
  {
    Source.OnChange.Listeners.RemoveListener(UpdateToken);
    Source.Value = Token.Value.ToString(WriteToSourceFormatting);
    Source.OnChange.Listeners.AddListener(UpdateToken);
  }
}
