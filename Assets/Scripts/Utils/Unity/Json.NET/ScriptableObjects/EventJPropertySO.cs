﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Newtonsoft.Json.Linq;

[CreateAssetMenu(menuName = "ScriptableObjects/EventJPropertySO")]
public class EventJPropertySO : EventBaseSO<JProperty>
{
  public override UnityEvent<JProperty> Listeners => _listeners;
  [SerializeField] UnityEventJProperty _listeners = new UnityEventJProperty();
}
