﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;
using Utils;
using UnityEditor;
using System.ComponentModel;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JObjectSO")]
public class JObjectSO : JContainerSO<JObject>
{
  public EventJTokenSO PropertyChanged = null;
  public EventJTokenSO PropertyChanging = null;

  public override void Register()
  {
    base.Register();
    Value.PropertyChanged += InvokePropertyChanged;
    Value.PropertyChanging += InvokePropertyChanging;
  }

  public override void Unregister()
  {
    base.Unregister();
    Value.PropertyChanged -= InvokePropertyChanged;
    Value.PropertyChanging -= InvokePropertyChanging;
  }

  public void InvokePropertyChanged(object sender,
    PropertyChangedEventArgs e) =>
    sender.As<JObject>()[e.PropertyName].As<JProperty>()
      .Pipe(PropertyChanged.Listeners.Invoke);

  public void InvokePropertyChanging(object sender,
    PropertyChangingEventArgs e) =>
    sender.As<JObject>()[e.PropertyName].As<JProperty>()
      .Pipe(PropertyChanged.Listeners.Invoke);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateJObjectEvents()
  {
    if (PropertyChanged == null)
    {
      PropertyChanged = ScriptableObject.CreateInstance<EventJTokenSO>();
      PropertyChanged.name = "PropertyChanged";
      AssetDatabase.AddObjectToAsset(PropertyChanged, this.GetPath());
    }

    if (PropertyChanging == null)
    {
      PropertyChanging = ScriptableObject.CreateInstance<EventJTokenSO>();
      PropertyChanging.name = "PropertyChanging";
      AssetDatabase.AddObjectToAsset(PropertyChanging, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
