﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;
using Utils;
using UnityEditor;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Linq;

public abstract class JTokenSOBase : ScriptableObject
{
  public SerializedObservable<JToken> Token
    = new SerializedObservable<JToken>(null);
#if UNITY_EDITOR
  [TextArea(4, 32)] public string StringRepresentation = "";
#endif

  public virtual void OnEnable()
  {
#if UNITY_EDITOR
    Token.OnChange += UpdateStringRepresentation;
#endif
  }

  public virtual void OnDisable()
  {
#if UNITY_EDITOR
    Token.OnChange -= UpdateStringRepresentation;
#endif
  }

#if UNITY_EDITOR
  public void UpdateStringRepresentation(JToken token) =>
    StringRepresentation = token == null ? "" : token.ToString();

  [ButtonMethod]
  public void UpdateStringRepresentation() =>
    UpdateStringRepresentation(Token.Value);

  [ButtonMethod]
  public void UpdateJValue()
  {
    var parsedToken = JToken.Parse(StringRepresentation);
    if (Token.Value is JValue) Token.Value.As<JValue>().Value = parsedToken;
  }
#endif

  public void Parse(string raw) => Token.Value = JToken.Parse(raw);

  [ButtonMethod]
  public void PrintType() => Debug.Log($"Type: {Token.Value.GetType().FullName}");
}

public abstract class JTokenSO<T> : JTokenSO where T : JToken
{
  public T Value => Token.Value as T;
}

public abstract class JContainerSO<T> : JTokenSO<T> where T : JContainer
{
  public EventObjectSO AddingNew = null;
  public EventNotifyCollectionChangedEventArgsSO CollectionChanged = null;
  public EventListChangedEventArgsSO ListChanged = null;

  public override void Register()
  {
    Value.AddingNew += InvokeAddingNew;
    Value.CollectionChanged += InvokeCollectionChanged;
    Value.ListChanged += InvokeListChanged;
  }

  public override void Unregister()
  {
    Value.AddingNew -= InvokeAddingNew;
    Value.CollectionChanged -= InvokeCollectionChanged;
    Value.ListChanged -= InvokeListChanged;
  }

  public void InvokeAddingNew(object sender, AddingNewEventArgs e) =>
    AddingNew.Invoke(e.NewObject);

  public void InvokeCollectionChanged(object sender,
    NotifyCollectionChangedEventArgs e) =>
    CollectionChanged.Invoke(e);

  public void InvokeListChanged(object sender, ListChangedEventArgs e) =>
    ListChanged.Invoke(e);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateComponents()
  {
    if (AddingNew == null)
    {
      AddingNew = ScriptableObject
        .CreateInstance<EventObjectSO>();
      AddingNew.name = "AddingNew";
      AssetDatabase.AddObjectToAsset(AddingNew, this.GetPath());
    }
    if (CollectionChanged == null)
    {
      CollectionChanged = ScriptableObject
        .CreateInstance<EventNotifyCollectionChangedEventArgsSO>();
      CollectionChanged.name = "CollectionChanged";
      AssetDatabase.AddObjectToAsset(CollectionChanged, this.GetPath());
    }
    if (ListChanged == null)
    {
      ListChanged = ScriptableObject
        .CreateInstance<EventListChangedEventArgsSO>();
      ListChanged.name = "ListChanged";
      AssetDatabase.AddObjectToAsset(ListChanged, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}

#if UNITY_EDITOR
[InitializeOnLoad]
public class JsonNETProcessor : UnityEditor.AssetModificationProcessor
{
  static string[] OnWillSaveAssets(string[] paths)
  {
    AssetDatabase.FindAssets($"t:{nameof(JTokenSOBase)}")
      .Select(AssetDatabase.GUIDToAssetPath)
      .Select(AssetDatabase.LoadAssetAtPath<JTokenSOBase>)
      .ForEach(ta =>
      {
        ta.OnDisable();
        ta.OnEnable();
        ta.UpdateStringRepresentation();
      });
    return paths;
  }
}
#endif