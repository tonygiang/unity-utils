﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JTokenSO")]
public class JTokenSO : JTokenSOBase
{
  public JTokenSOBase RelativeSource = null;
  public SerializedObservableString RelativePath
    = new SerializedObservableString("");

  public override void OnEnable()
  {
    base.OnEnable();
    Token.OnChange += Register;
    Token.OnBeforeChange += Unregister;
    RelativePath.OnChange += UpdateToken;
    RelativeSource.Token.OnChange += UpdateToken;
    UpdateToken();
  }

  public override void OnDisable()
  {
    base.OnDisable();
    Token.OnChange -= Register;
    Token.OnBeforeChange -= Unregister;
    RelativePath.OnChange -= UpdateToken;
    RelativeSource.Token.OnChange -= UpdateToken;
  }

  void UpdateToken(JToken token) => UpdateToken();
  void UpdateToken(string _) => UpdateToken();

  [ButtonMethod]
  public void UpdateToken()
  {
    if (RelativeSource == null) return;
    if (RelativeSource.Token.Value == null) return;
    Token.Value = RelativeSource.Token.Value.SelectToken(RelativePath.Value);
  }

  void Register(JToken token)
  {
    if (Token.Value == null) return;
    Register();
  }
  void Unregister(JToken token)
  {
    if (Token.Value == null) return;
    Unregister();
  }
  public virtual void Register() { }
  public virtual void Unregister() { }

  public void SetRelativePath(string value) => RelativePath.Value = value;

  public void CopyReferenceFrom(JTokenComp comp)
  {
    RelativeSource.Token.OnChange -= UpdateToken;
    RelativeSource = comp.RelativeSource;
    RelativeSource.Token.OnChange += UpdateToken;
    RelativePath.SetForceEvent(comp.RelativePath.Value);
  }

  public void CopyReferenceFrom(JTokenSO so)
  {
    RelativeSource.Token.OnChange -= UpdateToken;
    RelativeSource = so.RelativeSource;
    RelativeSource.Token.OnChange += UpdateToken;
    RelativePath.SetForceEvent(so.RelativePath.Value);
  }
}
