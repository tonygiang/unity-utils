﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Newtonsoft.Json.Linq;

[CreateAssetMenu(menuName = "ScriptableObjects/EventJTokenSO")]
public class EventJTokenSO : EventBaseSO<JToken>
{
  public override UnityEvent<JToken> Listeners => _listeners;
  [SerializeField] UnityEventJToken _listeners = new UnityEventJToken();
}
