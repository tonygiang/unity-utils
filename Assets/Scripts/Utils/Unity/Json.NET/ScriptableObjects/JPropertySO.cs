﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JPropertySO")]
public class JPropertySO : JContainerSO<JProperty>
{
}
