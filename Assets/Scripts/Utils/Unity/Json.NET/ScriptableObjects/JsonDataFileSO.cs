﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JsonDataFileSO")]
public class JsonDataFileSO : JTokenSOBase
{
  public PersistentDataFileSO File = null;
  public Formatting FormattingMode = Formatting.None;

  public override void OnEnable()
  {
    base.OnEnable();
    Token.Value = JToken.Parse(System.IO.File.ReadAllText(File.Path));
  }

  [ButtonMethod]
  public void Save() => System.IO.File.WriteAllText(File.Path,
    Token.Value.ToString(FormattingMode));
}
