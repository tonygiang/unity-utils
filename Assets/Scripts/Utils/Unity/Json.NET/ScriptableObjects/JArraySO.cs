﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

[CreateAssetMenu(menuName = "ScriptableObjects/Json.NET/JArraySO")]
public class JArraySO : JContainerSO<JArray>
{
}
