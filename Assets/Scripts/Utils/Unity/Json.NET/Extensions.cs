﻿using Newtonsoft.Json.Linq;

public static partial class Extensions
{
  public static T Ensure<T>(this T source, object key, JToken value)
    where T : JToken
  {
    if (source[key] == null) source[key] = value;
    return source;
  }
}
