﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;
using System.ComponentModel;

public class JObjectComp : JContainerComp<JObject>
{
  public EventJTokenInstance PropertyChanged = null;
  public EventJTokenInstance PropertyChanging = null;

  public override void Register()
  {
    base.Register();
    Value.PropertyChanged += InvokePropertyChanged;
    Value.PropertyChanging += InvokePropertyChanging;
  }

  public override void Unregister()
  {
    base.Unregister();
    if (Value == null) return;

    Value.PropertyChanged -= InvokePropertyChanged;
    Value.PropertyChanging -= InvokePropertyChanging;
  }

  public void InvokePropertyChanged(object sender,
    PropertyChangedEventArgs e) =>
    sender.As<JObject>()[e.PropertyName].As<JProperty>()
      .Pipe(PropertyChanged.Event.Invoke);

  public void InvokePropertyChanging(object sender,
    PropertyChangingEventArgs e) =>
    sender.As<JObject>()[e.PropertyName].As<JProperty>()
      .Pipe(PropertyChanged.Event.Invoke);

  [ButtonMethod]
  public void GenerateJObjectEvents()
  {
    if (PropertyChanged == null)
    {
      PropertyChanged = new GameObject("PropertyChanged")
        .AddComponent<EventJTokenInstance>();
      PropertyChanged.transform.SetParent(transform);
      PropertyChanged.transform.localPosition = Vector3.zero;
    }

    if (PropertyChanging == null)
    {
      PropertyChanging = new GameObject("PropertyChanging")
        .AddComponent<EventJTokenInstance>();
      PropertyChanging.transform.SetParent(transform);
      PropertyChanging.transform.localPosition = Vector3.zero;
    }

  }
}
