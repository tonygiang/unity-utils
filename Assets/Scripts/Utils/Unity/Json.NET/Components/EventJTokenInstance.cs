﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Newtonsoft.Json.Linq;

public class EventJTokenInstance : EventInstance<JToken>
{
  public override UnityEvent<JToken> Event => _event;
  [SerializeField] UnityEventJToken _event = new UnityEventJToken();
}
