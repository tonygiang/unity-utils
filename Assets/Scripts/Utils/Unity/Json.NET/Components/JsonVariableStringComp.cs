﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

public class JsonVariableStringComp : JTokenCompBase
{
  public VariableString Source = null;

  void Awake()
  {
    Source.OnChange.Listeners.AddListener(UpdateToken);
    UpdateToken(Source.Value);
  }

  void OnDestroy() =>
    Source.OnChange.Listeners.RemoveListener(UpdateToken);

  void UpdateToken(string value) => Token.Value = JToken.Parse(value);
}
