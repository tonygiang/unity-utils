﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;
using System.ComponentModel;
using System.Collections.Specialized;
using System;

public abstract class JTokenCompBase : MonoBehaviour
{
  public SerializedObservable<JToken> Token
    = new SerializedObservable<JToken>(null);
#if UNITY_EDITOR
  [TextArea(4, 32)] public string StringRepresentation = "";
#endif

  protected virtual void Awake()
  {
#if UNITY_EDITOR
    Token.OnChange += UpdateStringRepresentation;
#endif
  }

  protected virtual void OnDestroy()
  {
#if UNITY_EDITOR
    Token.OnChange -= UpdateStringRepresentation;
#endif
  }

#if UNITY_EDITOR
  public void UpdateStringRepresentation(JToken token) =>
    StringRepresentation = token == null ? "" : token.ToString();

  [ButtonMethod]
  public void UpdateStringRepresentation() =>
    UpdateStringRepresentation(Token.Value);

  [ButtonMethod]
  public void UpdateTokenFromStringRepresentation() =>
    Token.Value = JToken.Parse(StringRepresentation);
#endif

  public void Parse(string raw) => Token.Value = JToken.Parse(raw);
}

public abstract class JTokenComp<T> : JTokenComp where T : JToken
{
  public T Value => Token.Value as T;
}

public abstract class JContainerComp<T> : JTokenComp<T> where T : JContainer
{
  public EventObjectInstance AddingNew = null;
  public EventNotifyCollectionChangedEventArgsInstance CollectionChanged = null;
  public EventListChangedEventArgsInstance ListChanged = null;

  public override void Register()
  {
    Value.AddingNew += InvokeAddingNew;
    Value.CollectionChanged += InvokeCollectionChanged;
    Value.ListChanged += InvokeListChanged;
  }

  public override void Unregister()
  {
    Value.AddingNew -= InvokeAddingNew;
    Value.CollectionChanged -= InvokeCollectionChanged;
    Value.ListChanged -= InvokeListChanged;
  }

  public void InvokeAddingNew(object sender, AddingNewEventArgs e) =>
    AddingNew.Invoke(e.NewObject);

  public void InvokeCollectionChanged(object sender,
    NotifyCollectionChangedEventArgs e) =>
    CollectionChanged.Invoke(e);

  public void InvokeListChanged(object sender, ListChangedEventArgs e) =>
    ListChanged.Invoke(e);

  [ButtonMethod]
  public void GenerateComponents()
  {
    if (AddingNew == null)
    {
      AddingNew = new GameObject("AddingNew")
        .AddComponent<EventObjectInstance>();
      AddingNew.transform.SetParent(transform);
      AddingNew.transform.localPosition = Vector3.zero;
    }

    if (CollectionChanged == null)
    {
      CollectionChanged = new GameObject("CollectionChanged")
        .AddComponent<EventNotifyCollectionChangedEventArgsInstance>();
      CollectionChanged.transform.SetParent(transform);
      CollectionChanged.transform.localPosition = Vector3.zero;
    }

    if (ListChanged == null)
    {
      ListChanged = new GameObject("ListChanged")
        .AddComponent<EventListChangedEventArgsInstance>();
      ListChanged.transform.SetParent(transform);
      ListChanged.transform.localPosition = Vector3.zero;
    }
  }
}