﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

public class JsonTextAssetComp : JTokenCompBase
{
  public TextAsset Source = null;

  void OnEnable() => Token.Value = JToken.Parse(Source.text);
}
