﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using MyBox;

public class JTokenComp : JTokenCompBase
{
  public JTokenSOBase RelativeSource = null;
  public SerializedObservableString RelativePath
    = new SerializedObservableString("");

  protected override void Awake()
  {
    base.Awake();
    Token.OnChange += Register;
    Token.OnBeforeChange += Unregister;
    RelativePath.OnChange += UpdateToken;
    RelativeSource.Token.OnChange += UpdateToken;
    UpdateToken();
  }

  protected override void OnDestroy()
  {
    base.OnDestroy();
    Token.OnChange -= Register;
    Token.OnBeforeChange -= Unregister;
    RelativePath.OnChange -= UpdateToken;
    RelativeSource.Token.OnChange -= UpdateToken;
  }

  void UpdateToken(JToken _) => UpdateToken();
  void UpdateToken(string _) => UpdateToken();

  [ButtonMethod]
  public void UpdateToken()
  {
    if (RelativeSource == null) return;
    if (RelativeSource.Token.Value == null) return;
    Token.Value = RelativeSource.Token.Value.SelectToken(RelativePath.Value);
  }

  void Register(JToken _)
  {
    if (Token.Value == null) return;
    Register();
  }
  void Unregister(JToken _)
  {
    if (Token.Value == null) return;
    Unregister();
  }
  public virtual void Register() { }
  public virtual void Unregister() { }

  public void SetRelativePath(string value) => RelativePath.Value = value;

  public void CopyReferenceFrom(JTokenComp comp)
  {
    RelativeSource.Token.OnChange -= UpdateToken;
    RelativeSource = comp.RelativeSource;
    RelativeSource.Token.OnChange += UpdateToken;
    RelativePath.SetForceEvent(comp.RelativePath.Value);
  }

  public void CopyReferenceFrom(JTokenSO so)
  {
    RelativeSource.Token.OnChange -= UpdateToken;
    RelativeSource = so.RelativeSource;
    RelativeSource.Token.OnChange += UpdateToken;
    RelativePath.SetForceEvent(so.RelativePath.Value);
  }
}
