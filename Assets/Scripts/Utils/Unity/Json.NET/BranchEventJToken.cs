﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Newtonsoft.Json.Linq;
using System.Linq;

public class BranchEventJToken : MonoBehaviour
{
  public EventJTokenSO[] Sources = new EventJTokenSO[0];
  public SerializableDictionaryStringUnityEventJToken Events
    = new SerializableDictionaryStringUnityEventJToken();

  void Awake() => Sources.ForEach(s => s.Listeners.AddListener(Branch));

  void OnDestroy()
  {
    Sources.ForEach(s => s.Listeners.RemoveListener(Branch));
    Events = null;
  }

  void Branch(JToken value) => Events
    .Select(p => (Token: value.SelectToken(p.Key), Event: p.Value))
    .Where(t => t.Token != null)
    .ForEach(t => t.Event.Invoke(t.Token));
}
