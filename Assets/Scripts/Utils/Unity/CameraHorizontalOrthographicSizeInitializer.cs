﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.UI;

public class CameraHorizontalOrthographicSizeInitializer : ExtensionBehaviour<Camera>
{
  public AspectRatioFitter ReferenceFitter;
  public float CurrentRatio => BaseComp.pixelRect.width
    / BaseComp.pixelRect.height;
  public float TargetHorizontalSize;

  void Awake()
  {
    if (CurrentRatio >= ReferenceFitter.aspectRatio) return;
    BaseComp.orthographicSize = TargetHorizontalSize
      / Mathf.Min(CurrentRatio, ReferenceFitter.aspectRatio);
  }

  [ButtonMethod]
  public void Fill()
  {
    TargetHorizontalSize = BaseComp.orthographicSize
      * ReferenceFitter.aspectRatio;
  }
}
