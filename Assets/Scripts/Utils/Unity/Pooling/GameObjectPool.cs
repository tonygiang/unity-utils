﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

public class GameObjectPool : ObjectPool<GameObject>
{
  public GameObjectPoolSO Source = null;

  protected override void Awake()
  {
    base.Awake();
    if (Source != null) Source.Value = this;
  }

  public void EnableOnly(int number)
  {
    var activeInstances = Instances.Where(ActiveSelf);
    var activeCount = activeInstances.Count();
    if (activeCount > number) activeInstances.Take(activeCount - number)
      .ForEach(SetActiveFalse);
    else this.Get(InactiveSelf, number - activeCount).ForEach(SetActiveTrue);
  }

  public static bool ActiveSelf(GameObject source) => source.activeSelf;

  public static bool InactiveSelf(GameObject source) => !source.activeSelf;

  public static void SetActiveFalse(GameObject source) => source
    .SetActive(false);

  public static void SetActiveTrue(GameObject source) => source
    .SetActive(true);
}
