﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Utils;
using MyBox;
using System;

public class ObjectPool<T> : MonoBehaviour where T : UnityEngine.Object
{
  public T Prefab = null;
  public int PreCreatedNumber = 0;
  [PositiveValueOnly] public int GrowStep = 1;
  public Transform Container = null;
  public Queue<T> Instances = new Queue<T>();

  protected virtual void Awake() => this.Create(PreCreatedNumber);
}

public static class ObjectPoolExtensions
{
  public static T[] Create<T>(this ObjectPool<T> source, int number)
    where T : UnityEngine.Object
  {
    var newInstances = new T[number];
    for (int i = 0; i < number; ++i)
    {
      var newInstance = UnityEngine.Object.Instantiate(source.Prefab,
        source.Container);
      newInstances[i] = newInstance;
      source.Instances.Enqueue(newInstance);
    }
    return newInstances;
  }

  public static T[] CreateByStep<T>(this ObjectPool<T> source,
    int stepNumber) where T : UnityEngine.Object =>
    source.Create(source.GrowStep * stepNumber);

  public static IEnumerable<T> Get<T>(this ObjectPool<T> source,
    Func<T, bool> condition,
    int number = 1) where T : UnityEngine.Object
  {
    int matchedCount = 0;
    var matchedInstances = new List<T>();
    for (int remainingCount = source.Instances.Count;
      remainingCount >= number;
      --remainingCount)
    {
      var matchedInstance = source.Instances.Dequeue();
      source.Instances.Enqueue(matchedInstance);
      if (condition(matchedInstance))
      {
        matchedInstances.Add(matchedInstance);
        ++matchedCount;
      }
      if (matchedCount == number) return matchedInstances;
    }
    var stepsToGrow = Mathf.CeilToInt((float)number / source.GrowStep);
    return source.CreateByStep(stepsToGrow).Take(number);
  }

  public static T GetFirst<T>(this ObjectPool<T> source,
    Func<T, bool> condition) where T : UnityEngine.Object
  {
    int matchedCount = source.Instances.Count;
    T matchedInstance = null;
    do
    {
      matchedInstance = source.Instances.Dequeue();
      source.Instances.Enqueue(matchedInstance);
      if (condition(matchedInstance)) return matchedInstance;
      else --matchedCount;
    } while (matchedCount > 0);
    var newInstance = UnityEngine.Object.Instantiate(source.Prefab,
      source.Container);
    source.Instances.Enqueue(newInstance);
    return newInstance;
  }
}