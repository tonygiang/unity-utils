﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class DropdownFunctions : ExtensionBehaviour<Dropdown>
{
  public void ChangeValue(int delta) => BaseComp.value =
    (BaseComp.value + delta).ClampTo(0, BaseComp.options.Count - 1);

  public void InvokeOnChangeCurrent() => BaseComp.InvokeOnChangeCurrent();
}
