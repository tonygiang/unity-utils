﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[ExecuteAlways]
public class SafeAreaRectTransformSetter : ExtensionBehaviour<RectTransform>
{
  [ReadOnly] public bool IsInSimulator = false;
  public bool CoverBottom = true;

  void Awake() => SetToSafeArea();

  void OnRectTransformDimensionsChange()
  {
#if UNITY_EDITOR
    IsInSimulator = UnityEditor.DeviceSimulation.RunningStatus.Enabled;
#endif
    SetToSafeArea();
  }

  [ButtonMethod]
  public void SetToSafeArea()
  {
    if (IsInSimulator
      || !Utils.Unity.Constants.EnvironmentDefines["UNITY_EDITOR"])
    {
      var safeAreaRatio = new Vector2(
        Screen.safeArea.width / Screen.currentResolution.width,
        Screen.safeArea.height / Screen.currentResolution.height);
      var offsetRatio = new Vector2(
        Screen.safeArea.x / Screen.currentResolution.width,
        Screen.safeArea.y / Screen.currentResolution.height);
      if (CoverBottom) BaseComp.anchorMin = offsetRatio.SetY(0f);
      else BaseComp.anchorMin = offsetRatio;
      BaseComp.anchorMax = offsetRatio + safeAreaRatio;
    }
    else
    {
      BaseComp.anchorMin = Vector2.zero;
      BaseComp.anchorMax = Vector2.one;
      BaseComp.sizeDelta = Vector2.zero;
    }
  }
}
