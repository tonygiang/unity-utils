﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using MyBox;

public class RatioBasedGridLayoutGroup : LayoutGroup
{
  public RectOffsetRatio PaddingRatio;
  public Vector2 SizeRatio;
  public Vector2 SpacingRatio;
  Vector2 SizeWithSpacing => SizeRatio + SpacingRatio;
  public GridLayoutGroup.Constraint Constraint;
  [ConditionalField(nameof(Constraint),
    true,
    GridLayoutGroup.Constraint.Flexible)]
  [PositiveValueOnly] public int ConstraintCount = 1;

  public override void CalculateLayoutInputHorizontal()
  {
    base.CalculateLayoutInputHorizontal();
    var alignmentX = GetAlignmentOnAxis(0);
    var alignmentY = GetAlignmentOnAxis(1);
    var innerSpace = new Vector2(
      1f - PaddingRatio.Left - PaddingRatio.Right,
      1f - PaddingRatio.Top - PaddingRatio.Bottom);

    var effectiveColumns = ((innerSpace.x + SpacingRatio.x) / SizeWithSpacing.x)
      .Pipe(Mathf.FloorToInt);
    if (Constraint == GridLayoutGroup.Constraint.FixedColumnCount)
      effectiveColumns = ConstraintCount;
    var rowWidth = (effectiveColumns * SizeWithSpacing.x) - SpacingRatio.x;
    var startAnchorX = PaddingRatio.Left;
    if (alignmentX == 0.5f) startAnchorX = 0.5f - (rowWidth / 2);
    else if (alignmentX == 1f) startAnchorX = 1f - PaddingRatio.Right
      - rowWidth;

    var effectiveRows = ((innerSpace.y + SpacingRatio.y) / SizeWithSpacing.y)
      .Pipe(Mathf.FloorToInt);
    if (Constraint == GridLayoutGroup.Constraint.FixedRowCount)
      effectiveRows = ConstraintCount;
    var columnHeight = (effectiveRows * SizeWithSpacing.y) - SpacingRatio.y;
    var startAnchorY = 1 - PaddingRatio.Top;
    if (alignmentY == 0.5f) startAnchorY = 0.5f + (columnHeight / 2);
    else if (alignmentY == 1f) startAnchorY = PaddingRatio.Bottom
      + columnHeight;

    for (int i = 0; i < rectChildren.Count; ++i)
    {
      m_Tracker.Add(this,
        rectChildren[i],
        DrivenTransformProperties.SizeDelta
          | DrivenTransformProperties.Anchors);
      int columnIdx = 0, rowIdx = 0;
      if (Constraint == GridLayoutGroup.Constraint.FixedColumnCount ||
        Constraint == GridLayoutGroup.Constraint.Flexible)
      {
        columnIdx = i % effectiveColumns;
        rowIdx = i / effectiveColumns;
      }
      else if (Constraint == GridLayoutGroup.Constraint.FixedRowCount)
      {
        columnIdx = i / effectiveRows;
        rowIdx = i % effectiveRows;
      }
      rectChildren[i].anchorMin = new Vector2(
        startAnchorX + (columnIdx * SizeWithSpacing.x),
        startAnchorY - (rowIdx * SizeWithSpacing.y) - SizeRatio.y);
      rectChildren[i].anchorMax = new Vector2(
        startAnchorX + (columnIdx * SizeWithSpacing.x) + SizeRatio.x,
        startAnchorY - (rowIdx * SizeWithSpacing.y));
      rectChildren[i].sizeDelta = Vector2.zero;
    }
  }

  public override void CalculateLayoutInputVertical() { }

  public override void SetLayoutHorizontal() { }

  public override void SetLayoutVertical() { }
}

[Serializable]
public struct RectOffsetRatio
{
  public float Left;
  public float Right;
  public float Top;
  public float Bottom;
}