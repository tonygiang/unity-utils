﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyBox;

public class ImageFunctions : ExtensionBehaviour<Image>
{
  public VariableFloat BaseFullVariable = null;

  public void SetFillOnBaseFull(float value) =>
    BaseComp.fillAmount = value / BaseFullVariable.Value;

  public void SetColorHex(string hex) => BaseComp.color = hex.ToUnityColor();
}
