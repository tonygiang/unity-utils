﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using System.Linq;

[ExecuteAlways]
public class ProjectAndInitializeTransformPosition
  : ExtensionBehaviour<RectTransform>
{
  public Camera ReferenceCamera;
  [Layer] public int ReceiverLayer;
  [Tag] public string ReceiverTag;
  public Transform TargetTransform;

  void Awake() => SetPosition();

  void Start() { }

  void OnRectTransformDimensionsChange()
  {
#if UNITY_EDITOR
    SetPosition();
#endif
  }

  public void SetPosition()
  {
    if (!this.enabled) return;
    var wp = ReferenceCamera.WorldPointOffsetByDepth(BaseComp, 0f);
    var hits = Physics.RaycastAll(wp,
      ReferenceCamera.transform.forward,
      Mathf.Infinity,
      1 << ReceiverLayer)
      .Where(rch => rch.collider.gameObject.CompareTag(ReceiverTag));
    if (!hits.Any()) return;
    TargetTransform.position = hits.FirstOrDefault().point;
  }
}
