﻿using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasFitWidthComponent : MonoBehaviour
{
    [SerializeField] bool ComfortRatio = false;
    [ConditionalField("ComfortRatio", false, true)]
    [SerializeField] float targetRatio = 0.5625f;
    // Start is called before the first frame update

    public void Awake()
    {
        ResizeRectTransform();
    }

    private void ResizeRectTransform()
    {
        RectTransform rect = GetComponent<RectTransform>();
       
        float currentAspect = (float) Screen.width / (float) Screen.height;
        Debug.Log("Test Screen: Camera Aspect" + currentAspect + " " + Screen.width + " " + Screen.height);
        if (ComfortRatio && currentAspect > targetRatio)
        {
            rect.sizeDelta = new Vector2(1080, rect.sizeDelta.y);
        }
        else
        {
            //Debug.LogError("Test Screen: " + this.name + " " + Screen.width);
            rect.sizeDelta = new Vector2(1080 * currentAspect / targetRatio, rect.sizeDelta.y);
        }
    }
}
