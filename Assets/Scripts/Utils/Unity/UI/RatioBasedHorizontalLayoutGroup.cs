﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Utils;

public class RatioBasedHorizontalLayoutGroup : LayoutGroup
{
  public RectOffsetRatio PaddingRatio;
  public float SpacingRatio;
  public bool ControlChildAnchors;
  public bool ChildForceExpandWidth;

  public override void CalculateLayoutInputHorizontal()
  {
    base.CalculateLayoutInputHorizontal();
    var alignmentX = GetAlignmentOnAxis(0);
    var drivenProperties = DrivenTransformProperties.None;
    if (ControlChildAnchors) drivenProperties =
      DrivenTransformProperties.AnchorMinX
        | DrivenTransformProperties.AnchorMaxX;
    var availableWidthRatio = 1f - PaddingRatio.Left - PaddingRatio.Right;
    var totalSpacingRatio = (rectChildren.Count - 1) * SpacingRatio;
    var expandedWidthRatio = (availableWidthRatio - totalSpacingRatio)
      / rectChildren.Count;
    var totalWidthRatio = totalSpacingRatio + (ChildForceExpandWidth ?
      expandedWidthRatio * rectChildren.Count :
      rectChildren.Sum(r => r.GetAnchorDelta().x));
    var startAnchorX = PaddingRatio.Left;
    if (alignmentX == 0.5f) startAnchorX = 0.5f - (totalWidthRatio / 2);
    else if (alignmentX == 1f) startAnchorX = 1f - PaddingRatio.Right
      - totalWidthRatio;

    var totalWidthRatioSoFar = 0f;
    for (int i = 0; i < rectChildren.Count; ++i)
    {
      m_Tracker.Add(this, rectChildren[i], drivenProperties);
      var effectiveWidthRatio = ChildForceExpandWidth ?
        expandedWidthRatio :
        rectChildren[i].GetAnchorDelta().x;
      rectChildren[i].anchorMin = new Vector2(
        startAnchorX + totalWidthRatioSoFar,
        rectChildren[i].anchorMin.y);
      rectChildren[i].anchorMax = new Vector2(
        startAnchorX + totalWidthRatioSoFar + effectiveWidthRatio,
        rectChildren[i].anchorMin.y);
      totalWidthRatioSoFar += effectiveWidthRatio + SpacingRatio;
    }
  }

  public override void CalculateLayoutInputVertical() { }

  public override void SetLayoutHorizontal() { }

  public override void SetLayoutVertical() { }
}
