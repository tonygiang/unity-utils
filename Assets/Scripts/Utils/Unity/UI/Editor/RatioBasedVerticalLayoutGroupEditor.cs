﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(RatioBasedVerticalLayoutGroup))]
public class RatioBasedVerticalLayoutGroupEditor : Editor
{
  private IEnumerable<SerializedProperty> exposedFields;

  private void OnEnable()
  {
    exposedFields = serializedObject.targetObject.GetType().GetFields()
      .Select(field => field.Name)
      .Select(serializedObject.FindProperty)
      .Where(prop => prop != null);
  }

  public override void OnInspectorGUI()
  {
    EditorGUILayout.PropertyField(
      serializedObject.FindProperty("m_ChildAlignment"),
      true);
    foreach (var property in exposedFields)
      EditorGUILayout.PropertyField(property, true);
    serializedObject.ApplyModifiedProperties();
  }
}
