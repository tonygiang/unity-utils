﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class AutoImagePixelsPerUnitByAspectRatioFitter
  : ExtensionBehaviour<Image>
{
  public AspectRatioFitter Fitter;
  public float CurrentRatio => (float)Screen.width / Screen.height;
  public float ReferencePixelsPerUnit = 1f;

  void Start() { }

  void OnRectTransformDimensionsChange()
  {
    if (!this.enabled) return;
    if (CurrentRatio < Fitter.aspectRatio)
      BaseComp.pixelsPerUnitMultiplier = ReferencePixelsPerUnit;
    else BaseComp.pixelsPerUnitMultiplier = ReferencePixelsPerUnit
      * CurrentRatio / Fitter.aspectRatio;
  }
}
