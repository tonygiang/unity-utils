﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Utils;

public class RatioBasedVerticalLayoutGroup : LayoutGroup
{
  public RectOffsetRatio PaddingRatio;
  public float SpacingRatio;
  public bool ControlChildAnchors;
  public bool ChildForceExpandHeight;

  public override void CalculateLayoutInputHorizontal()
  {
    base.CalculateLayoutInputHorizontal();
    var alignmentY = GetAlignmentOnAxis(1);
    var drivenProperties = DrivenTransformProperties.None;
    if (ControlChildAnchors) drivenProperties =
      DrivenTransformProperties.AnchorMinY
        | DrivenTransformProperties.AnchorMaxY;
    var availableHeightRatio = 1f - PaddingRatio.Top - PaddingRatio.Bottom;
    var totalSpacingRatio = (rectChildren.Count - 1) * SpacingRatio;
    var expandedHeightRatio = (availableHeightRatio - totalSpacingRatio)
      / rectChildren.Count;
    var totalHeightRatio = totalSpacingRatio + (ChildForceExpandHeight ?
      expandedHeightRatio * rectChildren.Count :
      rectChildren.Sum(r => r.GetAnchorDelta().y));
    var startAnchorY = 1f - PaddingRatio.Top;
    if (alignmentY == 0.5f) startAnchorY = 0.5f + (totalHeightRatio / 2);
    else if (alignmentY == 1f) startAnchorY = PaddingRatio.Bottom
      - totalHeightRatio;

    var totalHeightRatioSoFar = 0f;
    for (int i = 0; i < rectChildren.Count; ++i)
    {
      m_Tracker.Add(this, rectChildren[i], drivenProperties);
      var effectiveHeightRatio = ChildForceExpandHeight ?
        expandedHeightRatio :
        rectChildren[i].GetAnchorDelta().y;
      rectChildren[i].anchorMin = new Vector2(
        rectChildren[i].anchorMin.x,
        startAnchorY - totalHeightRatioSoFar - effectiveHeightRatio);
      rectChildren[i].anchorMax = new Vector2(
        rectChildren[i].anchorMax.x,
        startAnchorY - totalHeightRatioSoFar);
      totalHeightRatioSoFar += effectiveHeightRatio + SpacingRatio;
    }
  }

  public override void CalculateLayoutInputVertical() { }

  public override void SetLayoutHorizontal() { }

  public override void SetLayoutVertical() { }
}
