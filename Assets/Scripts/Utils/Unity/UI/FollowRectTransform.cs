﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;

public class FollowRectTransform : MonoBehaviour
{
  [AutoProperty] public new Transform transform;
  public RectTransform Target;
  RectTransform _rootCanvasTransform = null;
  public Camera ReferenceCamera;
  [PositiveValueOnly] public float DistanceFromCamera;
  public bool DistanceFromRaycast = false;
  public LayerMask LayersToRaycastDistance;

  void UpdateAction()
  {
    if (Target == null) return;
    if (!Target.hasChanged) return;
    Follow();
  }

  [ButtonMethod]
  public void Follow()
  {
    var canvasScreenRect = _rootCanvasTransform.rect.size
      * _rootCanvasTransform.localScale;
    Vector3 viewPortPosition = Target.position / canvasScreenRect;
    viewPortPosition.z = DistanceFromCamera;
    var targetProjection = ReferenceCamera
      .ViewportToWorldPoint(viewPortPosition);
    if (DistanceFromRaycast)
    {
      var raycastResult = Physics.Raycast(ReferenceCamera.transform.position,
        ReferenceCamera.To(targetProjection),
        out RaycastHit hitInfo,
        float.PositiveInfinity,
        LayersToRaycastDistance);
      if (!raycastResult) return;
      transform.position = hitInfo.point;
    }
    else transform.position = targetProjection;
  }

  void OnEnable()
  {
    _rootCanvasTransform = Target.GetRootCanvas().GetComponent<RectTransform>();
    GameLoopRunner.Instance.UpdateActions.Add(UpdateAction);
  }

  void OnDisable() => GameLoopRunner.Instance.UpdateRemovalQueue
    .Enqueue(UpdateAction);
}
