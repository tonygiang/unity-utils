using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RectTransformEvents : MonoBehaviour
{
  public UnityEvent OnRectTransformDimensionsChangeEvent = new UnityEvent();

  void OnRectTransformDimensionsChange() =>
    OnRectTransformDimensionsChangeEvent.Invoke();
}
