﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[ExecuteAlways]
public class PersistCameraHorizontalFOV : MonoBehaviour
{
  public Camera TargetCamera = null;
  public float CurrentRatio =>
    (float)TargetCamera.pixelWidth / TargetCamera.pixelHeight;
  public float PersistingHorizontalFOV;

  void Awake() => SetPersistingHorizontalFOV();

  void OnRectTransformDimensionsChange()
  {
#if UNITY_EDITOR
    SetPersistingHorizontalFOV();
#endif
  }

  public void SetPersistingHorizontalFOV()
  {
    TargetCamera.fieldOfView = Camera.HorizontalToVerticalFieldOfView(
      PersistingHorizontalFOV,
      CurrentRatio);
  }

  [ButtonMethod]
  public void Fill() => PersistingHorizontalFOV = Camera
    .HorizontalToVerticalFieldOfView(TargetCamera.fieldOfView, CurrentRatio);
}
