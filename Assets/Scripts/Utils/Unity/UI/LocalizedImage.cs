﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedImage : ExtensionBehaviour<Image>
{
  public SerializableDictionarySystemLanguageSprite Mappings
    = new SerializableDictionarySystemLanguageSprite();

  void Awake()
  {
    if (!Mappings.ContainsKey(Application.systemLanguage)) return;
    BaseComp.sprite = Mappings[Application.systemLanguage];
  }
}