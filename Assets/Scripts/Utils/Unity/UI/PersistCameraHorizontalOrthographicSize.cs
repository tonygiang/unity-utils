﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[ExecuteAlways]
public class PersistCameraHorizontalOrthographicSize : MonoBehaviour
{
  public Camera TargetCamera = null;
  public float CurrentRatio =>
    (float)TargetCamera.pixelWidth / TargetCamera.pixelHeight;
  public float PersistingHorizontalOrthographicSize;

  void Awake() => SetPersistingHorizontalOrthographicSize();

  void OnRectTransformDimensionsChange()
  {
#if UNITY_EDITOR
    SetPersistingHorizontalOrthographicSize();
#endif
  }

  public void SetPersistingHorizontalOrthographicSize()
  {
    TargetCamera.orthographicSize
      = PersistingHorizontalOrthographicSize / CurrentRatio;
  }

  [ButtonMethod]
  public void Fill() => PersistingHorizontalOrthographicSize = TargetCamera
    .orthographicSize * CurrentRatio;
}
