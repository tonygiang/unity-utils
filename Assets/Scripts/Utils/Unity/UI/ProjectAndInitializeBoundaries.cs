﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using System.Linq;

namespace PairMatch.MainPlay
{
  [ExecuteAlways]
  public class ProjectAndInitializeBoundaries
    : ExtensionBehaviour<RectTransform>
  {
    public Camera ReferenceCamera;
    [Layer] public int ReceiverLayer;
    [Tag] public string ReceiverTag;
    public Transform LeftBoundaryPivot;
    public Transform RightBoundaryPivot;
    public Transform UpperBoundaryPivot;
    public Transform SpawnVolume;
    Vector3[] corners = new Vector3[4];

    void Awake() => SetBoundaries();

    void Start() { }

    void OnRectTransformDimensionsChange()
    {
#if UNITY_EDITOR
      SetBoundaries();
#endif
    }

    [ButtonMethod]
    public void SetBoundaries()
    {
      if (!this.enabled) return;
      BaseComp.GetWorldCorners(corners);
      var receiverHits = new List<RaycastHit>();
      for (int i = 0; i < corners.Length; ++i)
      {
        var hits = Physics.RaycastAll(corners[i],
          ReferenceCamera.transform.forward,
          Mathf.Infinity,
          1 << ReceiverLayer)
          .Where(rch => rch.collider.gameObject.CompareTag(ReceiverTag));
        if (!hits.Any()) return;
        receiverHits.Add(hits.FirstOrDefault());
      }
      if (receiverHits.Count < 2) return;
      var targetBounds = receiverHits.Aggregate(
        new Bounds(receiverHits[0].point, Vector3.zero),
        (p, c) => { p.Encapsulate(c.point); return p; });
      LeftBoundaryPivot.position = targetBounds.center
        + (targetBounds.extents.x * Vector3.left);
      RightBoundaryPivot.position = targetBounds.center
        + (targetBounds.extents.x * Vector3.right);
      UpperBoundaryPivot.position = targetBounds.center
        + (targetBounds.extents.z * Vector3.forward);
      SpawnVolume.position = targetBounds.center.SetY(SpawnVolume.position.y);
      SpawnVolume.localScale = new Vector3((targetBounds.extents.x - 0.5f) * 2,
        SpawnVolume.localScale.y,
        (targetBounds.extents.z - 0.5f) * 2);
      Physics.SyncTransforms();
    }

    void OnDrawGizmos()
    {
      BaseComp.GetWorldCorners(corners);
      var receiverHits = new List<RaycastHit>();
      for (int i = 0; i < corners.Length; ++i)
      {
        var hits = Physics.RaycastAll(corners[i],
          ReferenceCamera.transform.forward,
          Mathf.Infinity,
          1 << ReceiverLayer)
          .Where(rch => rch.collider.gameObject.CompareTag(ReceiverTag));
        if (!hits.Any()) return;
        receiverHits.Add(hits.FirstOrDefault());
      }
      if (receiverHits.Count < 2) return;
      Gizmos.color = Color.cyan;
      receiverHits.ForEach(rh => Gizmos.DrawLine(ReferenceCamera.transform.position, rh.point));
    }
  }
}
