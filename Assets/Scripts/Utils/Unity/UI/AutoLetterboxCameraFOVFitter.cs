// Author: Tony Giang

using UnityEngine;
using UnityEngine.UI;
using MyBox;

/// <summary>
/// This component keeps a target camera view fit for UGUI letterboxing in
/// accordance to the aspect ratio of the fitter. If the new aspect ratio is
/// wider than the fitter's aspect ratio, the target camera's vertical FOV will
/// be kept consistent. If the new aspect ratio is taller than the fitter's
/// aspect ratio, the target camera's vertical FOV will be stretched.
/// </summary>
[ExecuteAlways]
public class AutoLetterboxCameraFOVFitter : MonoBehaviour
{
  [AutoProperty] public AspectRatioFitter Fitter = null;
  public Camera TargetCamera = null;
  public static float CurrentRatio => (float)Screen.width / Screen.height;
  public float ReferenceVerticalFOV;

  void OnRectTransformDimensionsChange()
  {
    if (CurrentRatio < Fitter.aspectRatio)
    {
      var referenceHorizontalFOV = Camera.VerticalToHorizontalFieldOfView(
        ReferenceVerticalFOV,
        Fitter.aspectRatio);
      TargetCamera.fieldOfView = Camera.HorizontalToVerticalFieldOfView(
        referenceHorizontalFOV,
        CurrentRatio);
    }
    else TargetCamera.fieldOfView = ReferenceVerticalFOV;
  }

  [ButtonMethod]
  public void Fill() => ReferenceVerticalFOV = TargetCamera.fieldOfView;
}