﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Linq;

public class AutoSpaceTransformContainer : MonoBehaviour
{
  public Transform[] ChildrenTransforms;
  public Vector3 LocalPositionSpacing;

  void Awake() { ResortChildrenTransform(); }

  public void ResortChildrenTransform()
  {
    ChildrenTransforms = transform.Cast<Transform>().ToArray();
    for (int i = 0; i < ChildrenTransforms.Length; ++i)
      ChildrenTransforms[i].localPosition
        = Vector3.zero + (i * LocalPositionSpacing);
  }

  void OnTransformChildrenChanged() { ResortChildrenTransform(); }


}
