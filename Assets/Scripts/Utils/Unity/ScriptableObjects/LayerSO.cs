﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/LayerSO")]
public class LayerSO : ScriptableObject
{
  [Layer] public int Value = 0;

  public void ApplyTo(GameObject go) => go.layer = Value;

  public void ApplyTo(Component comp) => ApplyTo(comp.gameObject);
}
