﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;

[CreateAssetMenu(menuName = "ScriptableObjects/AudioSourcePoolSO")]
public class AudioSourcePoolSO : ScriptableObject
{
  [field: SerializeField, OverrideLabel("On")]
  public bool On { get; set; } = true;
  public AudioSourcePool Value = null;
  public RangedFloat PitchRange = new RangedFloat(1f, 1f);
  public RangedFloat VolumeRange = new RangedFloat(1f, 1f);

  public void PlayClip(Vector3 audioPosition, AudioClip clip) =>
    PlayClip(audioPosition, clip, 1f);

  public void PlayClip(Vector3 audioPosition, AudioClip clip, float volume)
  {
    if (!this.On) return;
    var source = Value.GetFirst(AudioSourceNotPlaying);
    source.volume = VolumeRange.GetRandom() * volume;
    source.transform.position = audioPosition;
    source.clip = clip;
    source.pitch = PitchRange.GetRandom();
    source.Play();
  }

  public void PlayClip(AudioClip clip) =>
    PlayClip(Value.transform.position, clip);

  public void PlayClipIgnoreSpace(AudioClip clip) =>
    PlayClipIgnoreSpace(clip, 1f);

  public void PlayClipIgnoreSpace(AudioClip clip, float volume)
  {
    if (!this.On) return;
    var source = Value.GetFirst(AudioSourceNotPlaying);
    source.volume = VolumeRange.GetRandom() * volume;
    source.clip = clip;
    source.pitch = PitchRange.GetRandom();
    source.Play();
  }

  public static bool AudioSourceNotPlaying(AudioSource s) => !s.isPlaying;
}
