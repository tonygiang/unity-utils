﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ColorSettings")]
public class ColorSettings : ScriptableObject
{
  public Color Value = Color.white;
}
