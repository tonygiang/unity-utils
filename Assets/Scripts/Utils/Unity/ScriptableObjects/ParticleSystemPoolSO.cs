﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ParticleSystemPoolSO")]
public class ParticleSystemPoolSO : ScriptableObject
{
  public ParticleSystemPool Value = null;
}
