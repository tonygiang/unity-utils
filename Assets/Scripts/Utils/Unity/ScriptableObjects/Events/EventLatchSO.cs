﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEditor;
using Utils;

[CreateAssetMenu(menuName = "ScriptableObjects/EventLatchSO")]
public class EventLatchSO : ScriptableObject
{
  [field: SerializeField, OverrideLabel("Countdown Start")]
  public int CountdownStart { get; set; } = 1;
  [field: SerializeField, OverrideLabel("Current Countdown")]
  public int CurrentCountdown { get; set; } = 1;
  public EventSO OnConditionMet = null;

  public void Signal()
  { if (--CurrentCountdown == 0) OnConditionMet.Listeners.Invoke(); }

  public void Reset() => CurrentCountdown = CountdownStart;

  public void Reset(int newCountdown)
  { CountdownStart = newCountdown; Reset(); }

#if UNITY_EDITOR
  [Foldout("Initial Values"), SerializeField] int InitialCountdownStart = 1;
  [Foldout("Initial Values"), SerializeField] int InitialCurrentCountdown = 1;

  void OnEnable()
  {
    CountdownStart = InitialCountdownStart;
    CurrentCountdown = InitialCurrentCountdown;
  }

  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnConditionMet == null)
    {
      OnConditionMet = ScriptableObject.CreateInstance<EventSO>();
      OnConditionMet.name = "OnConditionMet";
      AssetDatabase.AddObjectToAsset(OnConditionMet, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
