﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class EventBaseSO<T> : ScriptableObject
{
  public abstract UnityEvent<T> Listeners { get; }

  public void Invoke(T value) => Listeners.Invoke(value);
}
