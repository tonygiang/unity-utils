﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventComponentSO")]
public class EventComponentSO : EventBaseSO<Component>
{
  public override UnityEvent<Component> Listeners => _listeners;
  [SerializeField] UnityEventComponent _listeners = new UnityEventComponent();
}
