﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventFloatSO")]
public class EventFloatSO : EventBaseSO<float>
{
  public override UnityEvent<float> Listeners => _listeners;
  [SerializeField] UnityEventFloat _listeners = new UnityEventFloat();

  public void Invoke(VariableFloat variable) => Listeners
    .Invoke(variable.Value);

  public void Invoke(string value) => Listeners.Invoke(float.Parse(value));
}
