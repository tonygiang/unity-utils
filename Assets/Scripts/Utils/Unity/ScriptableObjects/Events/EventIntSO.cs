﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventIntSO")]
public class EventIntSO : EventBaseSO<int>
{
  public override UnityEvent<int> Listeners => _listeners;
  [SerializeField] UnityEventInt _listeners = new UnityEventInt();

  public void Invoke(VariableInt variable) => Listeners.Invoke(variable.Value);

  public void Invoke(string value) => Listeners.Invoke(int.Parse(value));
}
