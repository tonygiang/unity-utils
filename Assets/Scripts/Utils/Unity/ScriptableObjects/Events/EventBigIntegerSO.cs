﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Numerics;

[CreateAssetMenu(menuName = "ScriptableObjects/EventBigFloatSO")]
public class EventBigIntegerSO : EventBaseSO<BigInteger>
{
  public override UnityEvent<BigInteger> Listeners => _listeners;
  [SerializeField] UnityEventBigInteger _listeners = new UnityEventBigInteger();

  public void Invoke(VariableBigInteger variable) => Listeners
    .Invoke(variable.Value);

  public void Invoke(string value) => Listeners.Invoke(BigInteger.Parse(value));
}
