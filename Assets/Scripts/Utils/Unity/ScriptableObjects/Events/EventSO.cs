﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/EventSO")]
public class EventSO : ScriptableObject
{
  public UnityEvent Listeners = new UnityEvent();

  [ButtonMethod] public void Invoke() => Listeners.Invoke();
}
