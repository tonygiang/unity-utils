﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Specialized;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventNotifyCollectionChangedEventArgs")]
public class EventNotifyCollectionChangedEventArgsSO
  : EventBaseSO<NotifyCollectionChangedEventArgs>
{
  public override UnityEvent<NotifyCollectionChangedEventArgs> Listeners =>
    _listeners;
  [SerializeField]
  UnityEventNotifyCollectionChangedEventArgs _listeners
    = new UnityEventNotifyCollectionChangedEventArgs();
}
