﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventObjectSO")]
public class EventObjectSO : EventBaseSO<object>
{
  public override UnityEvent<object> Listeners => _listeners;
  [SerializeField] UnityEventObject _listeners = new UnityEventObject();
}
