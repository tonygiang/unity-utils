﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventDoubleSO")]
public class EventDoubleSO : EventBaseSO<double>
{
  public override UnityEvent<double> Listeners => _listeners;
  [SerializeField] UnityEventDouble _listeners = new UnityEventDouble();

  public void Invoke(VariableDouble variable) => Listeners
    .Invoke(variable.Value);

  public void Invoke(string value) => Listeners.Invoke(double.Parse(value));
}
