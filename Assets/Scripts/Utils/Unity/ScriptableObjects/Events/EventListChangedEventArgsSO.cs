﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventListChangedEventArgs")]
public class EventListChangedEventArgsSO : EventBaseSO<ListChangedEventArgs>
{
  public override UnityEvent<ListChangedEventArgs> Listeners => _listeners;
  [SerializeField]
  UnityEventListChangedEventArgs _listeners
    = new UnityEventListChangedEventArgs();
}
