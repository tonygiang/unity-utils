﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEditor;
using Utils;

[CreateAssetMenu(menuName = "ScriptableObjects/EventStringLatchSO")]
public class EventStringLatchSO : ScriptableObject
{
  [field: SerializeField, OverrideLabel("Countdown Start")]
  public int CountdownStart { get; set; } = 1;
  [field: SerializeField, OverrideLabel("Current Countdown")]
  public int CurrentCountdown { get; set; } = 1;
  public EventStringSO OnConditionMet = null;
  public VariableString Argument = null;

  public void Signal(string value) { Argument.Value = value; Signal(); }

  [ButtonMethod]
  public void Signal()
  { if (--CurrentCountdown == 0) OnConditionMet.Invoke(Argument.Value); }

  public void Reset() => CurrentCountdown = CountdownStart;

  public void Reset(int newCountdown)
  { CountdownStart = newCountdown; Reset(); }

#if UNITY_EDITOR
  [Foldout("Initial Values"), SerializeField] int InitialCountdownStart = 1;
  [Foldout("Initial Values"), SerializeField] int InitialCurrentCountdown = 1;

  void OnEnable()
  {
    CountdownStart = InitialCountdownStart;
    CurrentCountdown = InitialCurrentCountdown;
  }

  [ButtonMethod]
  public void GenerateAssets()
  {
    if (OnConditionMet == null)
    {
      OnConditionMet = ScriptableObject.CreateInstance<EventStringSO>();
      OnConditionMet.name = "OnConditionMet";
      AssetDatabase.AddObjectToAsset(OnConditionMet, this.GetPath());
    }
    if (Argument == null)
    {
      Argument = ScriptableObject.CreateInstance<VariableString>();
      Argument.name = "Argument";
      AssetDatabase.AddObjectToAsset(Argument, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
