﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/TranslateEventStringToStringSO")]
public class TranslateEventStringToStringSO : ScriptableObject
{
  public SerializableDictionaryStringString Mappings
    = new SerializableDictionaryStringString();
  public EventStringSO OutputEvent = null;

  public void Translate(string value)
  {
    if (!Mappings.ContainsKey(value)) return;
    OutputEvent.Listeners.Invoke(Mappings[value]);
  }

  public void Translate(VariableString variable) => Translate(variable.Value);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OutputEvent == null)
    {
      OutputEvent = ScriptableObject.CreateInstance<EventStringSO>();
      OutputEvent.name = "OutputEvent";
      AssetDatabase.AddObjectToAsset(OutputEvent, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
