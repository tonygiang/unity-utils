﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventBoolSO")]
public class EventBoolSO : EventBaseSO<bool>
{
  public override UnityEvent<bool> Listeners => _listeners;
  [SerializeField] UnityEventBool _listeners = new UnityEventBool();

  public void Invoke(VariableBool variable) => Listeners.Invoke(variable.Value);

  public void Invoke(string value) => Listeners.Invoke(bool.Parse(value));
}
