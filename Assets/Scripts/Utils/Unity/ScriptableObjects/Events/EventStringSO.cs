﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/EventStringSO")]
public class EventStringSO : EventBaseSO<string>
{
  public override UnityEvent<string> Listeners => _listeners;
  [SerializeField] UnityEventString _listeners = new UnityEventString();

  public void Invoke(VariableString variable) => Listeners
    .Invoke(variable.Value);
}
