﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableFloatPlayerPrefs")]
public class VariableFloatPlayerPrefs : VariableFloat
{
  public float DefaultValue = 0f;
  public string Key = string.Empty;
  void OnEnable()
  {
    Ensure();
    if (OnChange) OnChange.Listeners.AddListener(SavePlayerPrefs);
  }

  void OnDisable()
  { if (OnChange) OnChange.Listeners.RemoveListener(SavePlayerPrefs); }

  void SavePlayerPrefs(float value) => PlayerPrefs.SetFloat(Key, value);

  [ButtonMethod]
  public void Delete() => PlayerPrefs.DeleteKey(this.Key);

  [ButtonMethod]
  public void Ensure() => Ensure(DefaultValue);

  public void Ensure(float value)
  {
    if (!PlayerPrefs.HasKey(Key)) SavePlayerPrefs(value);
    Refresh();
  }

  [ButtonMethod]
  public void Refresh() => _value = PlayerPrefs.GetFloat(Key);
}
