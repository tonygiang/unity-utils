﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableString")]
public class VariableString : VariableBase<string>
{
  protected override EventBaseSO<string> _onBeforeChange => OnBeforeChange;
  public EventStringSO OnBeforeChange = null;
  protected override EventBaseSO<string> _onChange => OnChange;
  public EventStringSO OnChange = null;

  public void Set(VariableString variable) => Value = variable.Value;

  public void SetTrim(string newValue) => Value = newValue.Trim();

  public void SetWithoutEvent(VariableString variable) =>
    SetWithoutEvent(variable.Value);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = ScriptableObject.CreateInstance<EventStringSO>();
      OnBeforeChange.name = "OnBeforeChange";
      AssetDatabase.AddObjectToAsset(OnBeforeChange, this.GetPath());
    }
    if (OnChange == null)
    {
      OnChange = ScriptableObject.CreateInstance<EventStringSO>();
      OnChange.name = "OnChange";
      AssetDatabase.AddObjectToAsset(OnChange, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }

  [TextArea(4, 32)] public string ExpandedValue = "";

  void OnValidate() => ExpandedValue = Value;
#endif
}
