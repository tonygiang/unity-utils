﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableStringPlayerPrefs")]
public class VariableStringPlayerPrefs : VariableString
{
  public string DefaultValue = string.Empty;
  public string Key = string.Empty;
  void OnEnable()
  {
    Ensure();
    if (OnChange) OnChange.Listeners.AddListener(SavePlayerPrefs);
  }

  void OnDisable()
  { if (OnChange) OnChange.Listeners.RemoveListener(SavePlayerPrefs); }

  void SavePlayerPrefs(string value) => PlayerPrefs.SetString(Key, value);

  [ButtonMethod]
  public void Delete() => PlayerPrefs.DeleteKey(this.Key);

  [ButtonMethod]
  public void Ensure() => Ensure(DefaultValue);

  public void Ensure(string value)
  {
    if (!PlayerPrefs.HasKey(Key)) SavePlayerPrefs(value);
    Refresh();
  }

  [ButtonMethod]
  public void Refresh() => _value = PlayerPrefs.GetString(Key);
}
