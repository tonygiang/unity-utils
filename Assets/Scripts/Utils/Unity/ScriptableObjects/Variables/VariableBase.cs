﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class VariableBase<T> : ScriptableObject
{
  [SerializeField] protected T _value = default;
  public virtual T Value
  {
    get => _value;
    set
    {
      if (EqualityComparer<T>.Default.Equals(_value, value)) return;
      _onBeforeChange?.Listeners.Invoke(_value);
      _value = value;
      _onChange?.Listeners.Invoke(value);
    }
  }
#if UNITY_EDITOR
  [TextArea(1, 8)] public string DeveloperDescription = "";
#endif
  protected virtual EventBaseSO<T> _onBeforeChange { get; }
  protected virtual EventBaseSO<T> _onChange { get; }

  public void Set(T newValue) => Value = newValue;

  public void SetWithoutEvent(T newValue) => _value = newValue;

  public void SetForceEvent(T newValue)
  {
    if (EqualityComparer<T>.Default.Equals(_value, newValue))
    {
      _onBeforeChange?.Listeners.Invoke(_value);
      _onChange?.Listeners.Invoke(Value);
    }
    else Value = newValue;
  }
}
