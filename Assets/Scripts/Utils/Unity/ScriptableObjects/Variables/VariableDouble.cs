﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableDouble")]
public class VariableDouble : VariableBase<double>
{
  protected override EventBaseSO<double> _onBeforeChange => OnBeforeChange;
  public EventDoubleSO OnBeforeChange = null;
  protected override EventBaseSO<double> _onChange => OnChange;
  public EventDoubleSO OnChange = null;

  public void Set(VariableFloat variable) => Value = variable.Value;

  public void Parse(string value) => Value = double.Parse(value);

  public void Change(float delta) => Value += delta;

  public void ParseForceEvent(string value) =>
    SetForceEvent(double.Parse(value));

  public void SetWithoutEvent(VariableDouble variable) =>
    SetWithoutEvent(variable.Value);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = ScriptableObject.CreateInstance<EventDoubleSO>();
      OnBeforeChange.name = "OnBeforeChange";
      AssetDatabase.AddObjectToAsset(OnBeforeChange, this.GetPath());
    }
    if (OnChange == null)
    {
      OnChange = ScriptableObject.CreateInstance<EventDoubleSO>();
      OnChange.name = "OnChange";
      AssetDatabase.AddObjectToAsset(OnChange, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
