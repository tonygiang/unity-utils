﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableIntPlayerPrefs")]
public class VariableIntPlayerPrefs : VariableInt
{
  public int DefaultValue = 0;
  public string Key = string.Empty;
  void OnEnable()
  {
    Ensure();
    if (OnChange) OnChange.Listeners.AddListener(SavePlayerPrefs);
  }

  void OnDisable()
  { if (OnChange) OnChange.Listeners.RemoveListener(SavePlayerPrefs); }

  void SavePlayerPrefs(int value) => PlayerPrefs.SetInt(Key, value);

  [ButtonMethod]
  public void Delete() => PlayerPrefs.DeleteKey(this.Key);

  [ButtonMethod]
  public void Ensure() => Ensure(DefaultValue);

  public void Ensure(int value)
  {
    if (!PlayerPrefs.HasKey(Key)) SavePlayerPrefs(value);
    Refresh();
  }

  [ButtonMethod]
  public void Refresh() => _value = PlayerPrefs.GetInt(Key);
}
