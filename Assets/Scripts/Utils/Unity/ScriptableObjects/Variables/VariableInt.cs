﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableInt")]
public class VariableInt : VariableBase<int>
{
  protected override EventBaseSO<int> _onBeforeChange => OnBeforeChange;
  public EventIntSO OnBeforeChange = null;
  protected override EventBaseSO<int> _onChange => OnChange;
  public EventIntSO OnChange = null;

  public void Set(VariableInt variable) => Value = variable.Value;

  public void Parse(string value) => Value = int.Parse(value);

  public void Change(int delta) => Value += delta;

  public void ParseForceEvent(string value) => SetForceEvent(int.Parse(value));

  public void SetWithoutEvent(VariableInt variable) =>
    SetWithoutEvent(variable.Value);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = ScriptableObject.CreateInstance<EventIntSO>();
      OnBeforeChange.name = "OnBeforeChange";
      AssetDatabase.AddObjectToAsset(OnBeforeChange, this.GetPath());
    }
    if (OnChange == null)
    {
      OnChange = ScriptableObject.CreateInstance<EventIntSO>();
      OnChange.name = "OnChange";
      AssetDatabase.AddObjectToAsset(OnChange, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
