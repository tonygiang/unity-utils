﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableBoolPlayerPrefs")]
public class VariableBoolPlayerPrefs : VariableBool
{
  public bool DefaultValue = false;
  public string Key = string.Empty;
  public int ValueForTrue = 1;
  public int ValueForFalse = 0;
  void OnEnable()
  {
    Ensure();
    if (OnChange) OnChange.Listeners.AddListener(SavePlayerPrefs);
  }

  void OnDisable()
  { if (OnChange) OnChange.Listeners.RemoveListener(SavePlayerPrefs); }

  void SavePlayerPrefs(bool value) => PlayerPrefs
    .SetInt(Key, value ? ValueForTrue : ValueForFalse);

  [ButtonMethod]
  public void Delete() => PlayerPrefs.DeleteKey(this.Key);

  [ButtonMethod]
  public void Ensure() => Ensure(DefaultValue);

  public void Ensure(bool value)
  {
    if (!PlayerPrefs.HasKey(Key)) SavePlayerPrefs(value);
    Refresh();
  }

  [ButtonMethod]
  public void Refresh() => _value = PlayerPrefs.GetInt(Key) == ValueForTrue;
}
