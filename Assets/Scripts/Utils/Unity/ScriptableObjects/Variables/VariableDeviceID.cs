﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableDeviceID")]
public class VariableDeviceID : VariableString
{
  void OnEnable() => _value = SystemInfo.deviceUniqueIdentifier;
}
