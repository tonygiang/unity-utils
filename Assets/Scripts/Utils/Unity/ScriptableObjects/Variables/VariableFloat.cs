﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableFloat")]
public class VariableFloat : VariableBase<float>
{
  protected override EventBaseSO<float> _onBeforeChange => OnBeforeChange;
  public EventFloatSO OnBeforeChange = null;
  protected override EventBaseSO<float> _onChange => OnChange;
  public EventFloatSO OnChange = null;

  public void Set(VariableFloat variable) => Value = variable.Value;

  public void Parse(string value) => Value = float.Parse(value);

  public void Change(float delta) => Value += delta;

  public void ParseForceEvent(string value) =>
    SetForceEvent(float.Parse(value));

  public void SetWithoutEvent(VariableFloat variable) =>
    SetWithoutEvent(variable.Value);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = ScriptableObject.CreateInstance<EventFloatSO>();
      OnBeforeChange.name = "OnBeforeChange";
      AssetDatabase.AddObjectToAsset(OnBeforeChange, this.GetPath());
    }
    if (OnChange == null)
    {
      OnChange = ScriptableObject.CreateInstance<EventFloatSO>();
      OnChange.name = "OnChange";
      AssetDatabase.AddObjectToAsset(OnChange, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
