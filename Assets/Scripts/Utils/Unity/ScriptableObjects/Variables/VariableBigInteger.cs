﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;
using System.Numerics;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableBigInteger")]
public class VariableBigInteger : VariableBase<BigInteger>
{
  [SerializeField] protected string _stringValue = 0.ToString();
  public override BigInteger Value
  {
    get => base.Value;
    set { base.Value = value; _stringValue = Value.ToString(); }
  }
  protected override EventBaseSO<BigInteger> _onBeforeChange => OnBeforeChange;
  public EventBigIntegerSO OnBeforeChange = null;
  protected override EventBaseSO<BigInteger> _onChange => OnChange;
  public EventBigIntegerSO OnChange = null;

  void OnEnable() => _value = BigInteger.Parse(_stringValue);

  void OnDisable() => _value = BigInteger.Parse(_stringValue);

  public void Set(VariableBigInteger variable) => Value = variable.Value;

  public void Parse(string value) => Value = BigInteger.Parse(value);

  public void Change(VariableBigInteger delta) => Value += delta.Value;

  public void SetWithoutEvent(VariableBigInteger variable) =>
    SetWithoutEvent(variable.Value);

  [ButtonMethod]
  public void SetValue() => _value = BigInteger.Parse(_stringValue);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = ScriptableObject.CreateInstance<EventBigIntegerSO>();
      OnBeforeChange.name = "OnBeforeChange";
      AssetDatabase.AddObjectToAsset(OnBeforeChange, this.GetPath());
    }
    if (OnChange == null)
    {
      OnChange = ScriptableObject.CreateInstance<EventBigIntegerSO>();
      OnChange.name = "OnChange";
      AssetDatabase.AddObjectToAsset(OnChange, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
