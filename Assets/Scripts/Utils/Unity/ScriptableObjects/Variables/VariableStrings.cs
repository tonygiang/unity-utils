﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableStrings")]
public class VariableStrings : ScriptableObject
{
  public string[] Values = new string[0];
}
