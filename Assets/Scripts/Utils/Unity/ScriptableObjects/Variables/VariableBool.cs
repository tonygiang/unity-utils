﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableBool")]
public class VariableBool : VariableBase<bool>
{
  protected override EventBaseSO<bool> _onBeforeChange => OnBeforeChange;
  public EventBoolSO OnBeforeChange = null;
  protected override EventBaseSO<bool> _onChange => OnChange;
  public EventBoolSO OnChange = null;

  public void Set(VariableBool variable) => Value = variable.Value;

  public void Parse(string value) => Value = bool.Parse(value);

  public void Toggle() => Value = !Value;

  public void ParseForceEvent(string value) => SetForceEvent(bool.Parse(value));

  public void SetWithoutEvent(VariableBool variable) =>
    SetWithoutEvent(variable.Value);

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = ScriptableObject.CreateInstance<EventBoolSO>();
      OnBeforeChange.name = "OnBeforeChange";
      AssetDatabase.AddObjectToAsset(OnBeforeChange, this.GetPath());
    }
    if (OnChange == null)
    {
      OnChange = ScriptableObject.CreateInstance<EventBoolSO>();
      OnChange.name = "OnChange";
      AssetDatabase.AddObjectToAsset(OnChange, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
