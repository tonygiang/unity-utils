﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "ScriptableObjects/Engine")]
public class Engine : ScriptableObject
{
  public void DebugLog(string msg) => Debug.Log(msg);

  public void DebugLog(object msg) => Debug.Log(msg);
  // GameObject will do too but Transform is always guaranteed to live inside a
  // scene
  public void UnloadCurrentScene(Transform t) => SceneManager
    .UnloadSceneAsync(t.gameObject.scene);

  public void DeletePlayerPrefs(string key) => PlayerPrefs.DeleteKey(key);

  public void SetTimeScale(float value) => Time.timeScale = value;

  public new void Destroy(UnityEngine.Object obj) =>
    UnityEngine.Object.Destroy(obj);

  public void Destroy(GameObject obj) => UnityEngine.Object.Destroy(obj);

  public void DestroyImmediate(GameObject obj) =>
    UnityEngine.Object.DestroyImmediate(obj);

  public void Toggle(GameObject obj) => obj.SetActive(!obj.activeSelf);

  public void OpenURL(string url) => Application.OpenURL(url);

  public void Quit(int exitCode) => Application.Quit(exitCode);

  public void Vibrate() => Handheld.Vibrate();

#if UNITY_EDITOR
  public void SetAssetDirty(Object target) =>
    UnityEditor.EditorUtility.SetDirty(target);

  public void SaveAssets() => UnityEditor.AssetDatabase.SaveAssets();
#endif

  public void TurnOffBehaviour(UnityEngine.Object obj) =>
    TurnOffBehaviour((Behaviour)obj);

  public void TurnOffBehaviour(Behaviour b) => b.enabled = false;

  public void StopParticleSystem(UnityEngine.Object obj) =>
    StopParticleSystem((ParticleSystem)obj);

  public void StopParticleSystem(ParticleSystem ps) => ps.Stop();

  public void PauseParticleSystem(UnityEngine.Object obj) =>
    PauseParticleSystem((ParticleSystem)obj);

  public void PauseParticleSystem(ParticleSystem ps) => ps.Pause();

  public void PlayParticleSystem(UnityEngine.Object obj) =>
    PlayParticleSystem((ParticleSystem)obj);

  public void PlayParticleSystem(ParticleSystem ps) => ps.Play();
}
