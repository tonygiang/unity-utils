﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/GameObjectSO")]
public class GameObjectSO : ScriptableObject
{
  public GameObject Value = null;
}
