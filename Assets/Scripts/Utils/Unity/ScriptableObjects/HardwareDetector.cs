﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

[CreateAssetMenu(menuName = "ScriptableObjects/HardwareDetector")]
public class HardwareDetector : ScriptableObject
{
  public SerializableDictionaryRangedIntUnityEventInt OnDetectSystemMemorySize
    = new SerializableDictionaryRangedIntUnityEventInt();
  public string[] ApplicableDefines = new string[]
  {
    "UNITY_ANDROID",
    "UNITY_IOS"
  };
  IEnumerable<string> EffectiveDefines => ApplicableDefines
    .Intersect(Utils.Unity.Constants.EnvironmentDefines.Keys);

  void OnEnable()
  {
    if (EffectiveDefines.Any(d => Utils.Unity.Constants.EnvironmentDefines[d]))
    {
      OnDetectSystemMemorySize
        .Where(p => SystemInfo.systemMemorySize.InRangeInclusive(p.Key))
        .ForEach(p => p.Value.Invoke(SystemInfo.systemMemorySize));
    }
  }
}
