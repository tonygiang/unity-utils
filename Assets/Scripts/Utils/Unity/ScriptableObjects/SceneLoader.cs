﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MyBox;
using Utils;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/SceneLoader")]
public class SceneLoader : ScriptableObject
{
  public EventStringSO OnBeginLoad = null;
  public EventStringLatchSO OnFinishLoad = null;
  public EventStringSO OnBeginUnload = null;
  [field: SerializeField, OverrideLabel("Mode")]
  public LoadSceneMode Mode { get; set; } = LoadSceneMode.Additive;

  public void LoadActive(string sceneName)
  {
    var sceneToLoad = SceneManager.GetSceneByName(sceneName);
    if (sceneToLoad.isLoaded)
    {
      SceneManager.SetActiveScene(sceneToLoad);
      OnFinishLoad.Signal(sceneName);
      return;
    }
    OnBeginLoad.Listeners.Invoke(sceneName);
    SceneManager.LoadSceneAsync(sceneName, this.Mode).completed += asyncOp =>
    {
      SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
      OnFinishLoad.Signal(sceneName);
    };
  }

  public void LoadInBackground(string sceneName)
  {
    var sceneToLoad = SceneManager.GetSceneByName(sceneName);
    if (sceneToLoad.isLoaded) return;
    SceneManager.LoadSceneAsync(sceneName, this.Mode);
  }

  public void UnloadSceneOf(Transform t)
  {
    OnBeginUnload.Listeners.Invoke(t.gameObject.scene.name);
    SceneManager.UnloadSceneAsync(t.gameObject.scene);
  }

#if UNITY_EDITOR
  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeginLoad == null)
    {
      OnBeginLoad = ScriptableObject.CreateInstance<EventStringSO>();
      OnBeginLoad.name = "OnBeginLoad";
      AssetDatabase.AddObjectToAsset(OnBeginLoad, this.GetPath());
    }
    if (OnFinishLoad == null)
    {
      OnFinishLoad = ScriptableObject.CreateInstance<EventStringLatchSO>();
      OnFinishLoad.name = "OnFinishLoad";
      AssetDatabase.AddObjectToAsset(OnFinishLoad, this.GetPath());
    }
    if (OnBeginUnload == null)
    {
      OnBeginUnload = ScriptableObject.CreateInstance<EventStringSO>();
      OnBeginUnload.name = "OnBeginUnload";
      AssetDatabase.AddObjectToAsset(OnBeginUnload, this.GetPath());
    }

    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
  }
#endif
}
