﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;

[CreateAssetMenu(menuName = "ScriptableObjects/AudioSourceSO")]
public class AudioSourceSO : ScriptableObject
{
  public AudioSource Value = null;

  public void SetClip(AudioClip clip)
  {
    Value.Stop();
    Value.clip = clip;
    Value.Play();
  }

  public void Toggle(bool state) => Value?.Toggle(state);
}
