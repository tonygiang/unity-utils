﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
namespace UnityEditor.DeviceSimulation
{
  public class RunningStatus : DeviceSimulatorPlugin
  {
    public override string title => "Running Status";
    public static bool Enabled = false;

    public override void OnDestroy() => Enabled = false;

    public override UnityEngine.UIElements.VisualElement OnCreateUI()
    { Enabled = true; return null; }
  }
}
#endif
