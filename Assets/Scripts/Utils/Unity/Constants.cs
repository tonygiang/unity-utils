﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.Unity
{
  public partial class Constants
  {
    public readonly static Dictionary<string, bool> EnvironmentDefines
      = new Dictionary<string, bool>()
      {
        {
          "UNITY_EDITOR",
#if UNITY_EDITOR
          true
#else
          false
#endif
        },
        {
          "UNITY_ANDROID",
#if UNITY_ANDROID
          true
#else
          false
#endif
        },
        {
          "UNITY_IOS",
#if UNITY_IOS
          true
#else
          false
#endif
        },
        {
          "DEVELOPMENT_BUILD",
#if DEVELOPMENT_BUILD
          true
#else
          false
#endif
        }
      };

    public static WaitForFixedUpdate WaitForFixedUpdate =
      new WaitForFixedUpdate();

    public static WaitForEndOfFrame WaitForEndOfFrame = new WaitForEndOfFrame();

    /// <summary>
    /// A WaitForSeconds memoized generator function.
    /// </summary>
    /// <returns>A new WaitForSeconds if the matching seconds value has not been
    /// generator before, or a cached WaitForSeconds if the matching seconds
    /// value has been generated before</returns>
    public static MemoizedFunc<float, WaitForSeconds> WFSMemoizedGenerator
      = MemoizedFunc<float, WaitForSeconds>.From(s => new WaitForSeconds(s));

    /// <summary>
    /// A WaitForSecondsRealtime memoized generator function.
    /// </summary>
    /// <returns>A new WaitForSecondsRealtime if the matching seconds value has
    /// not been generator before, or a cached WaitForSecondsRealtime if the
    /// matching seconds value has been generated before</returns>
    public static MemoizedFunc<float, WaitForSecondsRealtime> WFSRTMemoizedGenerator
      = MemoizedFunc<float, WaitForSecondsRealtime>.From(s => new WaitForSecondsRealtime(s));
  }
}
