﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This component hooks into StateMachineBehaviour's callbacks to send events
/// up to the GameObject containing the Animator.
/// </summary>
public class StateSpecificEvents : StateMachineBehaviour
{
  public string[] OnEnterEventKeys = new string[0];
  public string[] OnExitEventKeys = new string[0];
  AnimatorFunctions af = null;

  // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
  override public void OnStateEnter(Animator animator,
    AnimatorStateInfo stateInfo,
    int layerIndex)
  {
    af = animator.GetComponent<AnimatorFunctions>();
    foreach (var k in OnEnterEventKeys) af.InvokeAnimationEvent(k);
  }

  // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
  override public void OnStateExit(Animator animator,
    AnimatorStateInfo stateInfo,
    int layerIndex)
  {
    foreach (var k in OnExitEventKeys) af.InvokeAnimationEvent(k);
  }
}
