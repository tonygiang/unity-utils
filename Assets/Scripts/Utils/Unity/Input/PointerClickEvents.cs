﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class PointerClickEvents : MonoBehaviour,
  IPointerDownHandler,
  IPointerUpHandler,
  IPointerClickHandler
{
  public int[] AllowedPointerIDs = new int[] { 0, -1 };
  public UnityEventPointerEventData OnPointerDownEvent
    = new UnityEventPointerEventData();
  public UnityEventPointerEventData OnPointerUpEvent
    = new UnityEventPointerEventData();
  public UnityEventPointerEventData OnPointerClickEvent
    = new UnityEventPointerEventData();

  public void OnPointerDown(PointerEventData eventData)
  {
    if (!AllowedPointerIDs.Contains(eventData.pointerId)) return;
    OnPointerDownEvent.Invoke(eventData);
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    if (!AllowedPointerIDs.Contains(eventData.pointerId)) return;
    OnPointerUpEvent.Invoke(eventData);
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    if (!AllowedPointerIDs.Contains(eventData.pointerId)) return;
    OnPointerUpEvent.Invoke(eventData);
  }
}
