﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class PointerDragEvents : MonoBehaviour,
  IBeginDragHandler,
  IEndDragHandler,
  IDragHandler
{
  public int[] AllowedPointerIDs = new int[] { 0, -1 };
  public UnityEventPointerEventData OnBeginDragEvent
    = new UnityEventPointerEventData();
  public UnityEventPointerEventData OnEndDragEvent
    = new UnityEventPointerEventData();
  public UnityEventPointerEventData OnDragEvent
    = new UnityEventPointerEventData();

  public void OnBeginDrag(PointerEventData eventData)
  {
    if (!AllowedPointerIDs.Contains(eventData.pointerId)) return;
    OnBeginDragEvent.Invoke(eventData);
  }

  public void OnEndDrag(PointerEventData eventData)
  {
    if (!AllowedPointerIDs.Contains(eventData.pointerId)) return;
    OnEndDragEvent.Invoke(eventData);
  }
  
  public void OnDrag(PointerEventData eventData)
  {
    if (!AllowedPointerIDs.Contains(eventData.pointerId)) return;
    OnDragEvent.Invoke(eventData);
  }
}
