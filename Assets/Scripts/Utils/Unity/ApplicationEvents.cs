﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ApplicationEvents : MonoBehaviour
{
  [SerializeField]
  SerializableDictionaryBoolUnityEventBool OnApplicationFocusEvent
    = new SerializableDictionaryBoolUnityEventBool();
  [SerializeField]
  SerializableDictionaryBoolUnityEventBool OnApplicationPauseEvent
    = new SerializableDictionaryBoolUnityEventBool();
  [SerializeField] UnityEvent OnApplicationQuitEvent = new UnityEvent();

  void OnApplicationFocus(bool hasFocus) =>
    OnApplicationFocusEvent[hasFocus].Invoke(hasFocus);

  void OnApplicationPause(bool pauseStatus) =>
    OnApplicationPauseEvent[pauseStatus].Invoke(pauseStatus);

  void OnApplicationQuit() => OnApplicationQuitEvent.Invoke();
}
