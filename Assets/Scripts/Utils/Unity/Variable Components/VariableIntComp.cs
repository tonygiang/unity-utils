﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class VariableIntComp : VariableBaseComp<int>
{
  protected override EventInstance<int> _onBeforeChange => OnBeforeChange;
  public EventIntInstance OnBeforeChange = null;
  protected override EventInstance<int> _onChange => OnChange;
  public EventIntInstance OnChange = null;

  public void Set(VariableIntComp variable) => Value = variable.Value;
  public void Set(VariableInt variable) => Value = variable.Value;
  public void Change(int delta) => Value += delta;

  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = new GameObject("OnBeforeChange")
        .AddComponent<EventIntInstance>();
      OnBeforeChange.transform.SetParent(transform);
      OnBeforeChange.transform.localPosition = Vector3.zero;
    }

    if (OnChange == null)
    {
      OnChange = new GameObject("OnChange").AddComponent<EventIntInstance>();
      OnChange.transform.SetParent(transform);
      OnChange.transform.localPosition = Vector3.zero;
    }
  }
}
