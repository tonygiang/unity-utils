﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;
using UnityEngine.Events;

public class EventListChangedEventArgsInstance : EventInstance<ListChangedEventArgs>
{
  public override UnityEvent<ListChangedEventArgs> Event => _event;
  [SerializeField]
  UnityEventListChangedEventArgs _event
    = new UnityEventListChangedEventArgs();
}
