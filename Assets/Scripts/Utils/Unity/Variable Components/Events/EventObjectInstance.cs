﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventObjectInstance : EventInstance<object>
{
  public override UnityEvent<object> Event => _event;
  [SerializeField] UnityEventObject _event = new UnityEventObject();
}
