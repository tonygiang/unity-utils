﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Specialized;
using UnityEngine.Events;

public class EventNotifyCollectionChangedEventArgsInstance
  : EventInstance<NotifyCollectionChangedEventArgs>
{
  public override UnityEvent<NotifyCollectionChangedEventArgs> Event =>
    _event;
  [SerializeField]
  UnityEventNotifyCollectionChangedEventArgs _event
    = new UnityEventNotifyCollectionChangedEventArgs();
}
