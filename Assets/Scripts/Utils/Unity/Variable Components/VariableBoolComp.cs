﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class VariableBoolComp : VariableBaseComp<bool>
{
  protected override EventInstance<bool> _onBeforeChange => OnBeforeChange;
  public EventBoolInstance OnBeforeChange = null;
  protected override EventInstance<bool> _onChange => OnChange;
  public EventBoolInstance OnChange = null;

  public void Set(VariableBoolComp variable) => Value = variable.Value;
  public void Set(VariableBool variable) => Value = variable.Value;

  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = new GameObject("OnBeforeChange")
        .AddComponent<EventBoolInstance>();
      OnBeforeChange.transform.SetParent(transform);
      OnBeforeChange.transform.localPosition = Vector3.zero;
    }

    if (OnChange == null)
    {
      OnChange = new GameObject("OnChange").AddComponent<EventBoolInstance>();
      OnChange.transform.SetParent(transform);
      OnChange.transform.localPosition = Vector3.zero;
    }
  }
}
