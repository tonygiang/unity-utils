using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

public class BranchEventFloatComp : MonoBehaviour
{
  public EventFloatInstance[] Sources = new EventFloatInstance[0];
  public SerializableDictionaryRangedFloatUnityEventFloat Events
    = new SerializableDictionaryRangedFloatUnityEventFloat();

  void Awake() => Sources.ForEach(s => s.Event.AddListener(Branch));

  void OnDestroy() => Sources.ForEach(s => s.Event?.RemoveListener(Branch));

  public void Branch(float value) => Events
    .Where(p => value.InRangeInclusive(p.Key))
    .ForEach(p => p.Value.Invoke(value));
  
  public void Branch(VariableFloatComp variable) => Branch(variable.Value);
}
