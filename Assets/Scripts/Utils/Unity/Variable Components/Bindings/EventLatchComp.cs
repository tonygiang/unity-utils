﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class EventLatchComp : MonoBehaviour
{
  [field: SerializeField, OverrideLabel("Countdown Start")]
  public int CountdownStart { get; set; } = 1;
  [field: SerializeField, OverrideLabel("Current Countdown")]
  public int CurrentCountdown { get; set; } = 1;
  public EventInstance OnConditionMet = null;

  void Awake() => Reset();

  [ButtonMethod]
  public void Signal()
  { if (--CurrentCountdown == 0) OnConditionMet.Invoke(); }

  public void Reset() => CurrentCountdown = CountdownStart;

  public void Reset(int newCountdown)
  { CountdownStart = newCountdown; Reset(); }

  public void ChangeCountdownStart(int delta) => CountdownStart += delta;

  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnConditionMet == null)
    {
      OnConditionMet = new GameObject("OnConditionMet")
        .AddComponent<EventInstance>();
      OnConditionMet.transform.SetParent(transform);
      OnConditionMet.transform.localPosition = Vector3.zero;
    }
  }
}
