﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using MyBox;

public class BranchEventIntComp : MonoBehaviour
{
  public EventIntInstance[] Sources = new EventIntInstance[0];
  public SerializableDictionaryRangedIntUnityEventInt Events
    = new SerializableDictionaryRangedIntUnityEventInt();

  void Awake() => Sources.ForEach(s => s.Event.AddListener(Branch));

  void OnDestroy() => Sources.ForEach(s => s.Event?.RemoveListener(Branch));

  public void Branch(int value) => Events
    .Where(p => value.InRangeInclusive(p.Key))
    .ForEach(p => p.Value.Invoke(value));

  public void Branch(VariableIntComp variable) => Branch(variable.Value);
}
