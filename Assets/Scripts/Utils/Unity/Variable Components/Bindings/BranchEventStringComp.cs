﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class BranchEventStringComp : MonoBehaviour
{
  public EventStringInstance[] Sources = new EventStringInstance[0];
  public SerializableDictionaryStringUnityEventString Events
    = new SerializableDictionaryStringUnityEventString();

  void Awake() => Sources.ForEach(s => s.Event.AddListener(Branch));

  void OnDestroy() => Sources.ForEach(s => s.Event?.RemoveListener(Branch));

  public void Branch(string value)
  {
    if (!Events.ContainsKey(value)) return;
    Events[value].Invoke(value);
  }

  public void Branch(VariableStringComp variable) => Branch(variable.Value);
}
