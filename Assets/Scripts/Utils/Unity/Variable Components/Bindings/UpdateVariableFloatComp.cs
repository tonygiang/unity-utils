using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class UpdateVariableFloatComp : MonoBehaviour
{
  public VariableFloatComp Target = null;
  [field: SerializeField, OverrideLabel("Factors")]
  public float Factors { get; set; } = 0f;

  void OnEnable() => GameLoopRunner.Instance.UpdateActions.Add(UpdateAction);

  void OnDisable() => GameLoopRunner.Instance.UpdateRemovalQueue
    .Enqueue(UpdateAction);

  public void UpdateAction() => Target.Value += Factors * Time.deltaTime;
}
