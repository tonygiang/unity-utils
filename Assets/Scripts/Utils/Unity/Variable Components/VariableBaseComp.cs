﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class VariableBaseComp<T> : MonoBehaviour
{
  [SerializeField] protected T _value = default;
  public virtual T Value
  {
    get => _value;
    set
    {
      if (EqualityComparer<T>.Default.Equals(_value, value)) return;
      _onBeforeChange?.Event.Invoke(value);
      _value = value;
      _onChange?.Event.Invoke(value);
    }
  }
#if UNITY_EDITOR
  [TextArea(1, 8)] public string DeveloperDescription = "";
#endif
  protected virtual EventInstance<T> _onBeforeChange { get; }
  protected virtual EventInstance<T> _onChange { get; }

  public void Set(T newValue) => Value = newValue;

  public void SetWithoutEvent(T newValue) => _value = newValue;

  public void SetForceEvent(T newValue)
  {
    if (EqualityComparer<T>.Default.Equals(_value, newValue))
    {
      _onBeforeChange?.Event.Invoke(Value);
      _onChange?.Event.Invoke(Value);
    }
    else Value = newValue;
  }
}
