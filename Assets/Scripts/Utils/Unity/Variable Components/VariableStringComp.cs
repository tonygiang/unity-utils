﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class VariableStringComp : VariableBaseComp<string>
{
  protected override EventInstance<string> _onBeforeChange => OnBeforeChange;
  public EventStringInstance OnBeforeChange = null;
  protected override EventInstance<string> _onChange => OnChange;
  public EventStringInstance OnChange = null;

  public void Set(VariableStringComp variable) => Value = variable.Value;
  public void Set(VariableString variable) => Value = variable.Value;

  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = new GameObject("OnBeforeChange")
        .AddComponent<EventStringInstance>();
      OnBeforeChange.transform.SetParent(transform);
      OnBeforeChange.transform.localPosition = Vector3.zero;
    }

    if (OnChange == null)
    {
      OnChange = new GameObject("OnChange").AddComponent<EventStringInstance>();
      OnChange.transform.SetParent(transform);
      OnChange.transform.localPosition = Vector3.zero;
    }
  }
}
