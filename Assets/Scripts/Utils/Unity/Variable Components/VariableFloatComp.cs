using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class VariableFloatComp : VariableBaseComp<float>
{
  protected override EventInstance<float> _onBeforeChange => OnBeforeChange;
  public EventFloatInstance OnBeforeChange = null;
  protected override EventInstance<float> _onChange => OnChange;
  public EventFloatInstance OnChange = null;

  public void Set(VariableFloatComp variable) => Value = variable.Value;
  public void Set(VariableFloat variable) => Value = variable.Value;
  public void Change(float delta) => Value += delta;

  [ButtonMethod]
  public void GenerateEvent()
  {
    if (OnBeforeChange == null)
    {
      OnBeforeChange = new GameObject("OnBeforeChange")
        .AddComponent<EventFloatInstance>();
      OnBeforeChange.transform.SetParent(transform);
      OnBeforeChange.transform.localPosition = Vector3.zero;
    }

    if (OnChange == null)
    {
      OnChange = new GameObject("OnChange").AddComponent<EventFloatInstance>();
      OnChange.transform.SetParent(transform);
      OnChange.transform.localPosition = Vector3.zero;
    }
  }
}
