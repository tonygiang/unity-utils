﻿using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using MyBox;
using UnityEngine.UI;
using UnityEditor;

namespace Utils
{
  public static partial class UnityFunctions
  {
    public static GameObject SetName(this GameObject source, string name)
    {
      source.name = name;
      return source;
    }

    public static T SetPosition<T>(this T source, Vector3 position)
      where T : Component
    {
      source.transform.position = position;
      return source;
    }

    public static float SignedAngleTo(this Vector2 from, Vector2 to) =>
      Vector2.SignedAngle(from, to);

    public static float SignedAngleTo(this Vector3 from, Vector2 to) =>
      Vector2.SignedAngle(from, to);

    public static Quaternion SetEulerZ(this Quaternion source, float z) =>
      Quaternion.Euler(source.eulerAngles.x, source.eulerAngles.y, z);

    public static Scene[] GetScenes()
    {
      var scenes = new Scene[SceneManager.sceneCount];
      for (int i = 0; i < SceneManager.sceneCount; ++i)
        scenes[i] = SceneManager.GetSceneAt(i);
      return scenes;
    }

    public static Vector2 LocalPointFromPointer(this RectTransform source,
      Vector2 pointerPosition)
    {
      var localPoint = Vector2.zero;
      RectTransformUtility.ScreenPointToLocalPointInRectangle(source,
        pointerPosition,
        Camera.main,
        out localPoint);
      return localPoint;
    }

    public static IEnumerable<Transform> SiblingSort(
      this IEnumerable<Transform> source, int siblingIndex)
    {
      source.Reverse().ForEach(t => t.SetSiblingIndex(siblingIndex));
      return source;
    }

    public static int GetRandom(this RangeInt source) => UnityEngine.Random
      .Range(source.start, source.start + source.length);

    public static float GetRandom(this RangedFloat range) =>
      UnityEngine.Random.Range(range.Min, range.Max);

    public static Vector3 Inverted(this Vector3 source) => source * -1;

    public static Vector3 Plus(this Vector3 source, Vector3 right) =>
      source + right;

    public static Vector3 Plus(this Vector3 source, float right) =>
      source + new Vector3(right, right, right);

    public static Vector3 Plus(this float source, Vector3 vector) =>
      vector + new Vector3(source, source, source);

    public static Vector2 Plus(this Vector2 source, Vector2 right) =>
      source + right;

    public static Vector2 Plus(this Vector2 source, float right) =>
      source + new Vector2(right, right);

    public static Vector2 Plus(this float source, Vector2 vector) =>
      vector + new Vector2(source, source);

    public static Vector3 Multiply(this Vector3 source, int right) =>
      source * right;

    public static Vector3 Multiply(this Vector3 source, float right) =>
      source * right;

    public static Vector3 Multiply(this float source, Vector3 right) =>
      source * right;

    public static Vector3 Multiply(this int source, Vector3 right) =>
      source * right;

    public static Vector2 Multiply(this Vector2 source, int right) =>
      source * right;

    public static Vector2 Multiply(this Vector2 source, float right) =>
      source * right;

    public static Vector2 Multiply(this float source, Vector2 right) =>
      source * right;

    public static Vector2 Multiply(this int source, Vector2 right) =>
      source * right;

    public static Color Plus(this Color source, Color right) =>
      source + right;

    public static Color Multiply(this Color source, int right) =>
      source * right;

    public static Color Multiply(this Color source, float right) =>
      source * right;

    public static Color Multiply(this float source, Color right) =>
      source * right;

    public static Vector2 Pow(this Vector2 source, float exponent) =>
      new Vector2(Mathf.Pow(source.x, exponent),
        Mathf.Pow(source.y, exponent));

    public static Vector3 Pow(this Vector3 source, float exponent) =>
      new Vector3(Mathf.Pow(source.x, exponent),
        Mathf.Pow(source.y, exponent),
        Mathf.Pow(source.z, exponent));

    public static Vector4 Pow(this Vector4 source, float exponent) =>
      new Vector4(Mathf.Pow(source.x, exponent),
        Mathf.Pow(source.y, exponent),
        Mathf.Pow(source.z, exponent),
        Mathf.Pow(source.w, exponent));

    public static Quaternion ToRotation(this Vector3 source) =>
      Quaternion.Euler(source);

    public static Vector3 RandomPoint(this Bounds source) => source.center
      + new Vector3(
          UnityEngine.Random.Range(-source.extents.x, source.extents.x),
          UnityEngine.Random.Range(-source.extents.y, source.extents.y),
          UnityEngine.Random.Range(-source.extents.z, source.extents.z));

    public static Material SetAlpha(this Material source, float alpha)
    {
      source.color = source.color.WithAlphaSetTo(alpha);
      return source;
    }

    public static Rigidbody ToggleConstraints(this Rigidbody source,
      RigidbodyConstraints constraints,
      bool state)
    {
      if (state) source.constraints = source.constraints | constraints;
      else source.constraints = source.constraints & ~constraints;
      return source;
    }

    public static Rigidbody2D ToggleConstraints(this Rigidbody2D source,
      RigidbodyConstraints2D constraints,
      bool state)
    {
      if (state) source.constraints = source.constraints | constraints;
      else source.constraints = source.constraints & ~constraints;
      return source;
    }

    public static Canvas GetRootCanvas(this RectTransform source)
    {
      var parentCanvases = source.GetComponentsInParent<Canvas>();
      return parentCanvases[parentCanvases.Length - 1];
    }

#if UNITY_EDITOR
    public static string GetPath(this UnityEngine.Object source) =>
      AssetDatabase.GetAssetPath(source);
#endif

    public static Dropdown InvokeOnChangeCurrent(this Dropdown source)
    { source.onValueChanged.Invoke(source.value); return source; }

    public static Dropdown SetForceEvent(this Dropdown source, int index)
    {
      if (source.value == index) source.InvokeOnChangeCurrent();
      else source.value = index;
      return source;
    }

    public static IEnumerable<T> FilterComponent<T>(
      this IEnumerable<GameObject> source) => source
      .Select(o => o.GetComponent<T>())
      .Where(c => c != null);

    public static IEnumerable<T> FilterComponent<T>(
      this IEnumerable<Component> source) => source
      .Select(o => o.GetComponent<T>())
      .Where(c => c != null);

    public static AudioSource Toggle(this AudioSource source, bool state)
    {
      if (state) source.UnPause(); else source.Pause();
      return source;
    }

    public static Vector2 GetUnscaledPixelSize(this RectTransform source)
    {
      RectTransform parentRect = null;
      if (source.parent == null) return source.sizeDelta;
      else if (source.parent.GetComponent<RectTransform>() == null)
        return source.sizeDelta;
      else parentRect = source.parent.GetComponent<RectTransform>();
      var parentSizeInPixel = parentRect.GetUnscaledPixelSize();
      var deltaAnchor = new Vector2(source.anchorMax.x - source.anchorMin.x,
        source.anchorMax.y - source.anchorMin.y);
      return (parentSizeInPixel * deltaAnchor) + source.sizeDelta;
    }

    public static RectTransform ShiftAnchor(this RectTransform source,
      float x,
      float y) => source.ShiftAnchor(new Vector2(x, y));

    public static RectTransform ShiftAnchor(this RectTransform source,
      Vector2 delta)
    {
      source.anchorMin += delta;
      source.anchorMax += delta;
      return source;
    }

    public static Vector2 GetAnchorCenter(this RectTransform source) =>
      (source.anchorMin + source.anchorMax) / 2;

    public static Vector2 GetAnchorDelta(this RectTransform source) =>
      source.anchorMax - source.anchorMin;

    public static Vector3 SetCameraDepthFrom(this Vector3 worldPos,
      Camera projectingCamera,
      float distance,
      Camera.MonoOrStereoscopicEye eye = Camera.MonoOrStereoscopicEye.Mono)
    {
      var screenPoint = projectingCamera.WorldToScreenPoint(worldPos, eye);
      return projectingCamera.ScreenToWorldPoint(screenPoint.SetZ(distance),
        eye);
    }

    public static Transform SetLossyScale(this Transform source,
      Vector3 targetLossyScale)
    {
      var scaleFactorsToTarget = Vector3.Scale(targetLossyScale,
        source.lossyScale.Pow(-1));
      source.localScale = Vector3.Scale(source.localScale,
        scaleFactorsToTarget);
      return source;
    }

    public static Vector3 WorldPointOffsetByDepth(this Camera camera,
      Vector3 source,
      float distanceFromCamera,
      Camera.MonoOrStereoscopicEye eye = Camera.MonoOrStereoscopicEye.Mono)
    {
      var screenPoint = camera.WorldToScreenPoint(source, eye);
      return camera.ScreenToWorldPoint(screenPoint.SetZ(distanceFromCamera),
        eye);
    }

    public static Vector3 WorldPointOffsetByDepth(this Camera camera,
      RectTransform rt,
      float distanceFromCamera,
      Camera.MonoOrStereoscopicEye eye = Camera.MonoOrStereoscopicEye.Mono)
    {
      var rootCanvas = rt.GetRootCanvas();
      var wp = rt.position;
      if (rootCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
      {
        var rootCanvasRect = rootCanvas.pixelRect;
        var viewportPoint = new Vector2(rt.position.x / rootCanvasRect.width,
          rt.position.y / rootCanvasRect.height);
        wp = camera.ViewportToWorldPoint(viewportPoint);
      }
      return camera.WorldPointOffsetByDepth(wp, distanceFromCamera, eye);
    }
  }
}