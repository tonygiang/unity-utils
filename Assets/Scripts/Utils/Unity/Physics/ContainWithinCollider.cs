﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;

public class ContainWithinCollider : MonoBehaviour
{
  [AutoProperty] public new Transform transform;
  [AutoProperty] public new Collider collider;
  public GameObjectSO ColliderGORef;
  Collider containerCollider;

  void Awake()
  {
    containerCollider = ColliderGORef.Value.GetComponent<Collider>();
  }

  void FixedUpdate()
  {
    if (!transform.hasChanged) return;
    if (containerCollider.bounds.Contains(transform.position)) return;
    var closestPoint = containerCollider.ClosestPoint(transform.position);
    transform.position = closestPoint.To(containerCollider.bounds.center)
      .normalized.Multiply(collider.bounds.extents.magnitude)
      .Plus(closestPoint);
  }
}
