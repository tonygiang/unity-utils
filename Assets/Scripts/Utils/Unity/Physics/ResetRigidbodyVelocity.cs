﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class ResetRigidbodyVelocity : ExtensionBehaviour<Rigidbody>
{
  public Vector3 DefaultVelocity;

  void FixedUpdateAction() => BaseComp.velocity = DefaultVelocity;
  
  void OnEnable() => GameLoopRunner.Instance.FixedUpdateActions
    .Add(FixedUpdateAction);

  void OnDisable() => GameLoopRunner.Instance.FixedUpdateRemovalQueue
    .Enqueue(FixedUpdateAction);
}
