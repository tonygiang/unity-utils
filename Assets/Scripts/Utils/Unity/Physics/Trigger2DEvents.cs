﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Linq;

public class Trigger2DEvents : MonoBehaviour
{
  [Tag] public string[] ApplicableTags = new string[0];
  [Layer] public int[] ApplicableLayers = new int[0];
  [SerializeField]
  UnityEventCollider2D OnTriggerEnter2DEvent = new UnityEventCollider2D();
  [SerializeField]
  UnityEventCollider2D OnTriggerExit2DEvent = new UnityEventCollider2D();
  [SerializeField]
  UnityEventCollider2D OnTriggerStay2DEvent = new UnityEventCollider2D();

  void OnTriggerEnter2D(Collider2D other)
  {
    if (!ApplicableTags.Contains(other.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    OnTriggerEnter2DEvent.Invoke(other);
  }

  void OnTriggerExit2D(Collider2D other)
  {
    if (!ApplicableTags.Contains(other.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    OnTriggerExit2DEvent.Invoke(other);
  }

  void OnTriggerStay2D(Collider2D other)
  {
    if (!ApplicableTags.Contains(other.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    OnTriggerStay2DEvent.Invoke(other);
  }
}
