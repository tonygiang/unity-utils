﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class RigidbodyFunctions : ExtensionBehaviour<Rigidbody>
{
  public void ToggleFreezeAll(bool state) => BaseComp.ToggleConstraints(
    RigidbodyConstraints.FreezeAll,
    state);

  public void ToggleFreezePositionX(bool state) => BaseComp.ToggleConstraints(
    RigidbodyConstraints.FreezePositionX,
    state);

  public void ToggleFreezePositionY(bool state) => BaseComp.ToggleConstraints(
    RigidbodyConstraints.FreezePositionY,
    state);

  public void ToggleFreezePositionZ(bool state) => BaseComp.ToggleConstraints(
    RigidbodyConstraints.FreezePositionZ,
    state);
}
