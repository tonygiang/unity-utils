﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Linq;

public class Collision2DEvents : MonoBehaviour
{
  [Tag] public string[] ApplicableTags = new string[0];
  [Layer] public int[] ApplicableLayers = new int[0];
  public RangedFloat ApplicableRelativeVelocity
    = new RangedFloat(float.MinValue, float.MaxValue);
  [SerializeField]
  UnityEventCollision2D OnCollisionEnter2DEvent = new UnityEventCollision2D();
  [SerializeField]
  UnityEventCollision2D OnCollisionExit2DEvent = new UnityEventCollision2D();
  [SerializeField]
  UnityEventCollision2D OnCollisionStay2DEvent = new UnityEventCollision2D();

  void OnCollisionEnter2D(Collision2D other)
  {
    if (!ApplicableTags.Contains(other.gameObject.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    if (!other.relativeVelocity.magnitude
      .InRangeInclusive(ApplicableRelativeVelocity)) return;
    OnCollisionEnter2DEvent.Invoke(other);
  }

  void OnCollisionExit2D(Collision2D other)
  {
    if (!ApplicableTags.Contains(other.gameObject.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    if (!other.relativeVelocity.magnitude
      .InRangeInclusive(ApplicableRelativeVelocity))
      return;
    OnCollisionExit2DEvent.Invoke(other);
  }

  void OnCollisionStay2D(Collision2D other)
  {
    if (!ApplicableTags.Contains(other.gameObject.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    if (!other.relativeVelocity.magnitude
      .InRangeInclusive(ApplicableRelativeVelocity)) return;
    OnCollisionStay2DEvent.Invoke(other);
  }
}
