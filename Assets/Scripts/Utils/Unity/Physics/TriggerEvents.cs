﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Linq;

public class TriggerEvents : MonoBehaviour
{
  [Tag] public string[] ApplicableTags = new string[0];
  [Layer] public int[] ApplicableLayers = new int[0];
  [SerializeField]
  UnityEventCollider OnTriggerEnterEvent = new UnityEventCollider();
  [SerializeField]
  UnityEventCollider OnTriggerExitEvent = new UnityEventCollider();
  [SerializeField]
  UnityEventCollider OnTriggerStayEvent = new UnityEventCollider();

  void OnTriggerEnter(Collider other)
  {
    if (!ApplicableTags.Contains(other.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    OnTriggerEnterEvent.Invoke(other);
  }

  void OnTriggerExit(Collider other)
  {
    if (!ApplicableTags.Contains(other.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    OnTriggerExitEvent.Invoke(other);
  }

  void OnTriggerStay(Collider other)
  {
    if (!ApplicableTags.Contains(other.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    OnTriggerStayEvent.Invoke(other);
  }
}
