﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Linq;

public class CollisionEvents : MonoBehaviour
{
  [Tag] public string[] ApplicableTags = new string[0];
  [Layer] public int[] ApplicableLayers = new int[0];
  public RangedFloat ApplicableRelativeVelocity
    = new RangedFloat(float.MinValue, float.MaxValue);
  [SerializeField]
  UnityEventCollision OnCollisionEnterEvent = new UnityEventCollision();
  [SerializeField]
  UnityEventCollision OnCollisionExitEvent = new UnityEventCollision();
  [SerializeField]
  UnityEventCollision OnCollisionStayEvent = new UnityEventCollision();

  void OnCollisionEnter(Collision other)
  {
    if (!ApplicableTags.Contains(other.gameObject.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    if (!other.relativeVelocity.magnitude
      .InRangeInclusive(ApplicableRelativeVelocity)) return;
    OnCollisionEnterEvent.Invoke(other);
  }

  void OnCollisionExit(Collision other)
  {
    if (!ApplicableTags.Contains(other.gameObject.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    if (!other.relativeVelocity.magnitude
      .InRangeInclusive(ApplicableRelativeVelocity))
      return;
    OnCollisionExitEvent.Invoke(other);
  }

  void OnCollisionStay(Collision other)
  {
    if (!ApplicableTags.Contains(other.gameObject.tag)) return;
    if (!ApplicableLayers.Contains(other.gameObject.layer)) return;
    if (!other.relativeVelocity.magnitude
      .InRangeInclusive(ApplicableRelativeVelocity)) return;
    OnCollisionStayEvent.Invoke(other);
  }
}
