﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMessageEmitter : MonoBehaviour
{
  public string OnEnterMessage = "";
  public string OnExitMessage = "";

  void OnTriggerEnter(Collider other)
  {
    other.GetComponent<EventStringInstance>()?.Invoke(OnEnterMessage);
  }

  void OnTriggerExit(Collider other)
  {
    other.GetComponent<EventStringInstance>()?.Invoke(OnExitMessage);
  }
}
