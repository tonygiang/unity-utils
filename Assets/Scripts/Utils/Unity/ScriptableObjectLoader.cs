﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectLoader : MonoBehaviour
{
  public ScriptableObject[] ScriptableObjects = new ScriptableObject[0];
}
