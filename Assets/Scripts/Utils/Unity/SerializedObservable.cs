﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializedObservable<T>
{
  [SerializeField] protected T _value = default(T);
  public virtual T Value
  {
    get => _value;
    set
    {
      if (EqualityComparer<T>.Default.Equals(_value, value)) return;
      OnBeforeChange.Invoke(_value);
      _value = value;
      OnChange.Invoke(value);
    }
  }
  public Action<T> OnBeforeChange = Constants<T>.NoOp;
  public Action<T> OnChange = Constants<T>.NoOp;

  public SerializedObservable(T initialValue) => _value = initialValue;

  public void SetWithoutEvent(T newValue) => _value = newValue;

  public void SetForceEvent(T newValue)
  {
    if (Value.Equals(newValue)) OnChange.Invoke(Value);
    else Value = newValue;
  }
}

[Serializable]
public class SerializedObservableBool : SerializedObservable<bool>
{
  public SerializedObservableBool(bool initialValue) : base(initialValue) { }
}

[Serializable]
public class SerializedObservableString : SerializedObservable<string>
{
  [SerializeField] UnityEventString _onBeforeChange = new UnityEventString();
  [SerializeField] UnityEventString _onChange = new UnityEventString();

  public SerializedObservableString(string initialValue) : base(initialValue)
  {
    OnBeforeChange += _onBeforeChange.Invoke;
    OnChange += _onChange.Invoke;
  }
}