﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[DisallowMultipleComponent]
[RequireComponent(typeof(TMP_InputField))]
public class TMPInputFieldFunctions : ExtensionBehaviour<TMP_InputField>
{
  public void SaveContentToPlayerPrefs(string key) =>
    PlayerPrefs.SetString(key, BaseComp.text);

  public void CopyContentToInputField(TMP_InputField targetField) =>
    targetField.text = BaseComp.text;

  public void LoadContentFromPlayerPrefs(string key) =>
    BaseComp.text = PlayerPrefs.HasKey(key) ?
      PlayerPrefs.GetString(key) :
      BaseComp.text;

  public void InvokeOnValueChange() => BaseComp.onValueChanged
    .Invoke(BaseComp.text);
}
