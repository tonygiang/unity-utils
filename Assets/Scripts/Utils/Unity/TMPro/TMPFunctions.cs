﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Utils;
using MyBox;

public class TMPFunctions : ExtensionBehaviour<TextMeshPro>
{
  [field: SerializeField, TextArea, OverrideLabel("Template")]
  public string Template { get; set; } = "{0}";
  public string FormatString = string.Empty;
  public SerializableNumberFormatInfo FormatInfo
    = new SerializableNumberFormatInfo();

  public void SetInt(int number) => BaseComp.text = string.Format(Template,
    number.ToString(FormatString, FormatInfo));

  public void SetInt(object number) => SetInt((int)number);
}
