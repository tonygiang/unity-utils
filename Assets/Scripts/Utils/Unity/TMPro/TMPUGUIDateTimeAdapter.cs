﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MyBox;
using System;

public class TMPUGUIDateTimeAdapter : ExtensionBehaviour<TextMeshProUGUI>
{
  [field: SerializeField, TextArea, OverrideLabel("Template")]
  public string Template { get; set; } = "{0}";
  public string FormatString = string.Empty;

  public void SetSeconds(float seconds)
  {
    var formattedString = TimeSpan.FromSeconds(seconds).ToString(FormatString);
    BaseComp.text = string.Format(Template, formattedString);
  }
}
