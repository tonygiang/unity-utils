﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  public static class Extensions
  {
    public static float GetPreferredHeight(this TMP_Text source) =>
      LayoutUtility.GetPreferredHeight(source.rectTransform);
  }
}