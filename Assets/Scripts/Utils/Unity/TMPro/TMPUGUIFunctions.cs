﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MyBox;
using System.Numerics;

public class TMPUGUIFunctions : ExtensionBehaviour<TextMeshProUGUI>
{
  [field: SerializeField, TextArea, OverrideLabel("Template")]
  public string Template { get; set; } = "{0}";
  public string FormatString = string.Empty;
  public SerializableNumberFormatInfo FormatInfo
    = new SerializableNumberFormatInfo();

  public void SetInt(int number) => BaseComp.text = string.Format(Template,
    number.ToString(FormatString, FormatInfo));

  public void SetInt(VariableInt variable) => SetInt(variable.Value);

  public void SetFloat(float number) => BaseComp.text = string.Format(Template,
    number.ToString(FormatString, FormatInfo));

  public void SetFloat(VariableFloat variable) => SetFloat(variable.Value);

  public void SetDouble(double number) => BaseComp.text = string.Format(
    Template,
    number.ToString(FormatString, FormatInfo));

  public void SetDouble(VariableDouble variable) => SetDouble(variable.Value);

  public void SetBigInteger(BigInteger number) => BaseComp.text = string
    .Format(Template, number.ToString());

  public void SetBigInteger(VariableBigInteger number) =>
    SetBigInteger(number.Value);

  public void SetFloatCeil(float number) => SetInt(Mathf.CeilToInt(number));

  public void ParseInt(string value) => SetInt(int.Parse(value));

  public void ParseFloat(string value) => SetFloat(float.Parse(value));

  public void SetTemplatedString(string value) => BaseComp.text = string
    .Format(Template, value);

  public void SetTemplatedString(VariableString variable) => SetTemplatedString(
    variable.Value);
}
