﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMPro
{
  public class OnLinkClick : ExtensionBehaviour<TextMeshProUGUI>,
    IPointerClickHandler
  {
    public Camera ActiveCamera = null;
    public UnityEventString Event = new UnityEventString();

    public void OnPointerClick(PointerEventData eventData)
    {
      int linkIndex = TMP_TextUtilities.FindIntersectingLink(BaseComp,
        eventData.position,
        ActiveCamera);
      if (linkIndex < 0) return;
      Event.Invoke(BaseComp.textInfo.linkInfo[linkIndex].GetLinkID());
    }
  }
}