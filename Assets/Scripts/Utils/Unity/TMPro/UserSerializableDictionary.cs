﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System;
using TMPro;

[Serializable]
public class SerializableDictionaryStringTMPInputField
  : SerializableDictionaryBase<string, TMP_InputField>
{ }