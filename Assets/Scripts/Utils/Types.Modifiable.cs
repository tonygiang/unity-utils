﻿using System.Collections.Generic;
using System.Linq;
using System;

/// <summary>
/// An encapsulation of a base value and the functions that modifies it.
/// </summary>
public partial class Modifiable<TKey, TValue>
{
  /// <summary>
  /// The base value of the Modifiable.
  /// </summary>
  public TValue BaseValue;
  /// <summary>
  /// The sorted functions that modifies the base value. The order in which they
  /// are applied is determined by their associated keys.
  /// </summary>
  public SortedList<TKey, Func<TValue, TValue>> Modifiers =
    new SortedList<TKey, Func<TValue, TValue>>();
  /// <summary>
  /// Returns the value aggregated by passing BaseValue to the functions
  /// contained in Modifiers in the ascending order of their keys. Each function
  /// takes the result of invoking the previous function as its argument,
  /// starting with BaseValue as the argument for the first function.
  /// </summary>
  public TValue EffectiveValue
  { get { return Modifiers.Values.Aggregate(BaseValue, Apply); } }

  /// <summary>
  /// Creates a Modifiable with the base value initialized to a specified value.
  /// </summary>
  /// <returns>A new Modifiable.</returns>
  public static Modifiable<TKey, TValue> From(TValue baseValue)
  { return new Modifiable<TKey, TValue> { BaseValue = baseValue }; }

  /// <summary>
  /// Creates a Modifiable from another Modifiable with the same base value
  /// type. The base value and modifiers of the new Modifiable are
  /// shallow-copied from the other Modifiable. A parameter specifies how to
  /// convert the other Modifiable's modifier key type to the new Modifiable's
  /// key type.
  /// </summary>
  /// <returns>A new Modifiable.</returns>
  public static Modifiable<TKey, TValue> From<TKeyOther>(
    Modifiable<TKeyOther, TValue> other,
    Func<TKeyOther, TKey> keyConverter)
  {
    var newMod = From(other.BaseValue);
    foreach (var otherMod in other.Modifiers)
      newMod.Modifiers.Add(keyConverter(otherMod.Key), otherMod.Value);
    return newMod;
  }

  /// <summary>
  /// Invokes modifier function using inputValue as the argument and returns the
  /// result.
  /// </summary>
  /// <returns>The result of invoking modifier using inputValue.</returns> 
  public static TValue Apply(TValue inputValue, Func<TValue, TValue> modifier)
  { return modifier.Invoke(inputValue); }
}