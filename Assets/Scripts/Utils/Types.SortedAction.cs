﻿// Author: Tony Giang

using System;
using System.Collections.Generic;

/// <summary>
/// An alias for SortedList&lt;TKey, Action&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey> : SortedList<TKey, Action> { }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T> : SortedList<TKey, Action<T>> { }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2>
  : SortedList<TKey, Action<T1, T2>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3>
  : SortedList<TKey, Action<T1, T2, T3>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4>
  : SortedList<TKey, Action<T1, T2, T3, T4>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>>
{ }

/// <summary>
/// An alias for SortedList&lt;TKey, Action&lt;T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16&gt;&gt;.
/// A SortedAction can be treated as a delegate that lets you control the 
/// order-of-execution of its callbacks through their associated keys.
/// </summary>
public partial class SortedAction<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>
  : SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>>
{ }

public static partial class ExtensionMethods
{
  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey>(this SortedList<TKey, Action> source)
  { foreach (var action in source.Values) action(); }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T>(
    this SortedList<TKey, Action<T>> source,
    T arg)
  { foreach (var action in source.Values) action(arg); }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2>(
    this SortedList<TKey, Action<T1, T2>> source,
    T1 arg1,
    T2 arg2)
  {
    foreach (var action in source.Values) action(arg1,
      arg2);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3>(
    this SortedList<TKey, Action<T1, T2, T3>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4>(
    this SortedList<TKey, Action<T1, T2, T3, T4>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10,
    T11 arg11)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10,
      arg11);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10,
    T11 arg11,
    T12 arg12)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10,
      arg11,
      arg12);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10,
    T11 arg11,
    T12 arg12,
    T13 arg13)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10,
      arg11,
      arg12,
      arg13);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10,
    T11 arg11,
    T12 arg12,
    T13 arg13,
    T14 arg14)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10,
      arg11,
      arg12,
      arg13,
      arg14);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10,
    T11 arg11,
    T12 arg12,
    T13 arg13,
    T14 arg14,
    T15 arg15)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10,
      arg11,
      arg12,
      arg13,
      arg14,
      arg15);
  }

  /// <summary>
  /// Invokes the contained actions in the ascending order of their keys.
  /// </summary>
  public static void Invoke<TKey, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(
    this SortedList<TKey, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>> source,
    T1 arg1,
    T2 arg2,
    T3 arg3,
    T4 arg4,
    T5 arg5,
    T6 arg6,
    T7 arg7,
    T8 arg8,
    T9 arg9,
    T10 arg10,
    T11 arg11,
    T12 arg12,
    T13 arg13,
    T14 arg14,
    T15 arg15,
    T16 arg16)
  {
    foreach (var action in source.Values) action(arg1,
      arg2,
      arg3,
      arg4,
      arg5,
      arg6,
      arg7,
      arg8,
      arg9,
      arg10,
      arg11,
      arg12,
      arg13,
      arg14,
      arg15,
      arg16);
  }
}