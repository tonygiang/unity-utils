﻿using System.Collections.Generic;
using System.Linq;
using System;

/// <summary>
/// An encapsulation of a base value and the functions that modifies it.
/// </summary>
public partial class ModifiableRef<TKey, TValue>
{
  /// <summary>
  /// The base value getter of the ModifiableRef.
  /// </summary>
  public Func<TValue> GetBaseValue;
  /// <summary>
  /// The sorted functions that modifies the base value. The order in which they
  /// are applied is determined by their associated keys.
  /// </summary>
  public SortedList<TKey, Func<TValue, TValue>> Modifiers =
    new SortedList<TKey, Func<TValue, TValue>>();
  /// <summary>
  /// Returns the value aggregated by passing the base value acquired from
  /// GetBaseValue to the functions contained in Modifiers in the ascending
  /// order of their keys. Each function takes the result of invoking the
  /// previous function as its argument, starting with BaseValue as the argument
  /// for the first function.
  /// </summary>
  public TValue EffectiveValue
  { get { return Modifiers.Values.Aggregate(GetBaseValue(), Apply); } }

  /// <summary>
  /// Creates a ModifiableRef with the base value initialized to a specified
  /// value.
  /// </summary>
  /// <returns>A new ModifiableRef.</returns>
  public static ModifiableRef<TKey, TValue> From(Func<TValue> baseValue)
  { return new ModifiableRef<TKey, TValue> { GetBaseValue = baseValue }; }

  /// <summary>
  /// Creates a ModifiableRef from another ModifiableRef with the same base
  /// value type. The base value getter and modifiers of the new Modifiable are
  /// copied from the other ModifiableRef. A parameter specifies how to convert
  /// the other ModifiableRef's modifier key type to the new ModifiableRef's key
  /// type.
  /// </summary>
  /// <returns>A new ModifiableRef.</returns>
  public static ModifiableRef<TKey, TValue> From<TKeyOther>(
    ModifiableRef<TKeyOther, TValue> other,
    Func<TKeyOther, TKey> keyConverter)
  {
    var newMod = From(other.GetBaseValue);
    foreach (var otherMod in other.Modifiers)
      newMod.Modifiers.Add(keyConverter(otherMod.Key), otherMod.Value);
    return newMod;
  }

  /// <summary>
  /// Invokes modifier function using inputValue as the argument and returns the
  /// result.
  /// </summary>
  /// <returns>The result of invoking modifier using inputValue.</returns> 
  public static TValue Apply(TValue inputValue, Func<TValue, TValue> modifier)
  { return modifier.Invoke(inputValue); }
}