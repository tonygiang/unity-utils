using System.Collections.Generic;
using System;
using System.Threading;
using System.Linq;
using MyBox;

namespace Utils
{
  public static class Algorithm
  {
    public static bool HasContent(this string source) =>
      !String.IsNullOrWhiteSpace(source);

    public static bool IsEmpty(this string source) =>
      String.IsNullOrWhiteSpace(source);

    public static int ToInt(this string str) => int.Parse(str);

    public static float ToFloat(this string str) => float.Parse(str);

    public static bool OnlyCharactersIn(this string source,
      string allowedChars) => source.ToCharArray().All(allowedChars.Contains);

    public static T ClampTo<T>(this T source, T minValue, T maxValue)
      where T : IComparable => source.CompareTo(minValue) < 0 ? minValue :
      source.CompareTo(maxValue) > 0 ? maxValue :
      source;

    public static int Difference(this int source, int target) =>
      Math.Abs(source - target);

    public static (T1, T2) MakeTuple<T1, T2>(this T1 item1, T2 item2) =>
      (item1, item2);

    public static (T1, T2, T3) MakeTuple<T1, T2, T3>(this T1 item1,
      T2 item2,
      T3 item3) =>
      (item1, item2, item3);

    public static (T1, T2) MakeTupleAppend<T1, T2>(this T2 item2, T1 item1) =>
      (item1, item2);

    public static (T1, T2, T3) MakeTupleAppend<T1, T2, T3>(this T3 item3,
      T1 item1,
      T2 item2) => (item1, item2, item3);

    public static (T1, T2) Pipe<T1, T2>(this (T1, T2) arguments,
      Action<T1, T2> action)
    {
      action(arguments.Item1, arguments.Item2);
      return arguments;
    }

    public static (T1, T2, T3) Pipe<T1, T2, T3>(this (T1, T2, T3) arguments,
      Action<T1, T2, T3> action)
    {
      action(arguments.Item1, arguments.Item2, arguments.Item3);
      return arguments;
    }

    public static R Pipe<T1, T2, R>(this (T1, T2) arguments,
      Func<T1, T2, R> function) =>
      function(arguments.Item1, arguments.Item2);

    public static R Pipe<T1, T2, T3, R>(this (T1, T2, T3) arguments,
      Func<T1, T2, T3, R> function) =>
      function(arguments.Item1, arguments.Item2, arguments.Item3);

    public static Nullable<T> NullIf<T>(this T source, bool condition)
      where T : struct =>
      condition ? null : new Nullable<T>(source);

    public static Nullable<T> NullIf<T>(this T source, Predicate<T> condition)
      where T : struct =>
      condition(source) ? null : new Nullable<T>(source);

    public static IDictionary<TKey, TValue> Replace<TKey, TValue>(
      this IDictionary<TKey, TValue> source,
      TKey key,
      TValue value,
      Action<TValue> actionForExistingValue = null)
    {
      if (source.ContainsKey(key)) actionForExistingValue?.Invoke(source[key]);
      source[key] = value;
      return source;
    }

    public static IEnumerable<T> FillToSize<T>(this IEnumerable<T> source,
      int size,
      Func<int, T> generator)
    {
      int sourceStartLength = source.Count();
      var additions = Enumerable.Range(sourceStartLength,
        size - sourceStartLength)
        .Select(generator);
      return source.Concat(additions);
    }

    public static ICollection<T> FillToSize<T>(this ICollection<T> source,
      int size,
      Func<int, T> generator)
    {
      int sourceStartLength = source.Count;
      for (int i = sourceStartLength; i < size; ++i)
        source.Add(generator(i));
      return source;
    }

    public static IEnumerable<int> IndicesWhere<T>(this IEnumerable<T> source,
      Predicate<T> predicate)
    {
      var indices = Enumerable.Empty<int>();
      int sourceLength = source.Count();
      for (int i = 0; i < sourceLength; ++i)
        if (predicate(source.ElementAt(i))) indices = indices.Append(i);
      return indices;
    }

    public static IEnumerable<T> Exclude<T>(this IEnumerable<T> source,
      T element) where T : class => source.Where(se => se != element);

    public static IEnumerable<C> CastType<T, C>(this IEnumerable<T> source) =>
      source.Where(e => e is C).Cast<C>();

    public static int Multiply(this int source, int right) => source * right;

    public static float Multiply(this int source, float right) =>
      source * right;

    public static float Multiply(this float source, float right) =>
      source * right;

    public static float Plus(this float source, float right) =>
      source + right;

    public static int Plus(this int source, int right) => source + right;

    public static float Sqrt(this float source) => (float)Math.Sqrt(source);

    public static float Pow(this float source, float power) =>
      (float)Math.Pow(source, power);

    public static int Round(this float source) => (int)Math.Round(source);

    public static int Floor(this float source) => (int)Math.Floor(source);

    public static IEnumerable<T> ValuesOf<T>() where T : System.Enum =>
      Enum.GetValues(typeof(T)).Cast<T>();

    public static IList<T> FillBy<T>(this IList<T> source,
      Func<int, T> valueFactory)
    {
      for (int i = 0; i < source.Count; ++i) source[i] = valueFactory(i);
      return source;
    }

    public static T[] FillBy<T>(this T[] source,
      Func<int, T> valueFactory)
    {
      for (int i = 0; i < source.Length; ++i) source[i] = valueFactory(i);
      return source;
    }

    public static T[] ExclusiveSample<T>(this IList<T> source,
      int sampleNumber) =>
      source.ExclusiveSample(sampleNumber,
        MyBox.MyCommonConstants.SystemRandom);

    public static T[] ExclusiveSample<T>(this IList<T> source,
      int sampleNumber,
      Random rng)
    {
      if (sampleNumber > source.Count)
        throw new ArgumentOutOfRangeException("Cannot sample more elements than what the source collection contains");
      var results = new T[sampleNumber];
      int resultIndex = 0;
      for (int i = 0; i < source.Count && resultIndex < sampleNumber; ++i)
      {
        var probability = (double)(sampleNumber - resultIndex)
          / (source.Count - i);
        if (rng.NextDouble() < probability)
        { results[resultIndex] = source[i]; ++resultIndex; }
      }
      return results;
    }

    public static IList<T> SwapInPlace<T>(this IList<T> source,
      int index1,
      int index2)
    {
      var e1 = source[index1];
      source[index1] = source[index2];
      source[index2] = e1;
      return source;
    }

    public static IList<T> KnuthShuffle<T>(this IList<T> source)
    {
      for (int i = 0; i < source.Count - 1; ++i)
      {
        var indexToSwap = UnityEngine.Random.Range(i, source.Count);
        source.SwapInPlace(i, indexToSwap);
      }
      return source;
    }

    public static IList<T> DoForElementAt<T>(this IList<T> source,
      int idx,
      Action<T> action)
    {
      if (idx < source.Count && idx >= 0) action(source[idx]);
      return source;
    }

    public static void CancelAndDispose(ref CancellationTokenSource cts)
    {
      cts?.Cancel();
      cts?.Dispose();
      cts = null;
    }
  }

  public static class Algorithm2
  {
    public static T NullIf<T>(this T source, bool condition)
      where T : class =>
      condition ? null : source;

    public static T NullIf<T>(this T source, Predicate<T> condition)
      where T : class =>
      condition(source) ? null : source;
  }
}