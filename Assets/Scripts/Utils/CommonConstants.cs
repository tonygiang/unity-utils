﻿public partial class Constants
{
  public static char[] ColonArray = new char[] { ',' };
  public static char[] VerticalBarArray = new char[] { '|' };
  public static char[] HyphenArray = new char[] { '-' };
  public static char VerticalBar = '|';
  public static char Colon = ':';
  public static char ZeroChar = '0';
}
