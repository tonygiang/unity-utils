﻿using System;
using System.Collections.Generic;

[Serializable]
public class Observable<T>
{
  protected T _value = default;
  public virtual T Value
  {
    get => _value;
    set
    {
      T oldValue = _value;
      if (EqualityComparer<T>.Default.Equals(_value, value)) return;
      _value = value;
      OnChange(oldValue, value);
    }
  }
  public Action<T, T> OnChange = Constants<T, T>.NoOp;

  public static readonly Observable<T> Default = new Observable<T>(default);

  public Observable(T initialValue) => _value = initialValue;
}
